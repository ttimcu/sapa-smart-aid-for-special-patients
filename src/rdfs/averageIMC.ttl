@prefix ex-geo:           <http://example.org/geo#> .
@prefix eg:               <http://example.org/ns#> .
@prefix qb:               <http://purl.org/linked-data/cube#> .
@prefix dct:              <http://purl.org/dc/terms/> .
@prefix xsd:              <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf:              <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:             <http://www.w3.org/2000/01/rdf-schema#> .
@prefix foaf:             <http://xmlns.com/foaf/0.1/> .
@prefix schemaorg:        <http://schema.org/> .
@prefix admingeo:         <http://data.ordnancesurvey.co.uk/ontology/admingeo/> .
@prefix interval:         <http://reference.data.gov.uk/def/intervals/> .
@prefix sdmx-subject:     <http://purl.org/linked-data/sdmx/2009/subject#> .
@prefix sdmx-attribute:   <http://purl.org/linked-data/sdmx/2009/attribute#> .
@prefix sdmx-dimension:   <http://purl.org/linked-data/sdmx/2009/dimension#> .
@prefix sdmx-measure:     <http://purl.org/linked-data/sdmx/2009/measure#> .
@prefix sdmx-code:        <http://purl.org/linked-data/sdmx/2009/code#> .
@prefix org:              <http://www.w3.org/ns/org#> .


# -- Data Set --------------------------------------------

eg:dataset-sapa1 a qb:DataSet;
    dct:title       "Average IMC"@en;
    rdfs:label      "Average IMC"@en;
    rdfs:comment    "Average IMC sorted by gender, age and bearth place"@en;
    dct:description "Average IMC sorted by gender, age and bearth place"@en;
    dct:publisher   eg:organization;
    dct:issued      "2017-01-11"^^xsd:date;
    dct:subject
        sdmx-subject:1.4;      # Health
    qb:structure eg:dsd-sapa1 ;  
    sdmx-attribute:unitMeasure <http://dbpedia.org/page/Kilogram> ;
    qb:slice eg:slice1, eg:slice2, eg:slice3, eg:slice4, eg:slice5, eg:slice6 ;
    .

eg:organization a org:Organization, foaf:Agent;
    rdfs:label "SAPA"@en .


# -- Data structure definition ----------------------------

eg:dsd-sapa1 a qb:DataStructureDefinition;
    qb:component 
        [ qb:dimension eg:refArea;         qb:order 1 ],
        [ qb:dimension eg:refAge;          qb:order 2; qb:componentAttachment qb:Slice ],
        [ qb:dimension sdmx-dimension:sex; qb:order 3; qb:componentAttachment qb:Slice ];
    
    qb:component [ qb:measure eg:averageWeight];
    qb:component [ qb:attribute sdmx-attribute:unitMeasure; 
                   qb:componentRequired "true"^^xsd:boolean;
                   qb:componentAttachment qb:DataSet; ] ;

    qb:sliceKey eg:sliceByRegion ;
    .
    
eg:sliceByRegion a qb:SliceKey;
    rdfs:label "slice by region"@en;
    rdfs:comment "Slice by grouping regions together, fixing sex and age values"@en;
    qb:componentProperty eg:refAge, sdmx-dimension:sex ;
    .


# -- Dimensions and measures  ----------------------------

eg:refAge  a rdf:Property, qb:DimensionProperty;
    rdfs:label "reference age"@en;
    rdfs:subPropertyOf sdmx-dimension:age;
    rdfs:range interval:Interval;
    .

eg:refArea  a rdf:Property, qb:DimensionProperty;
    rdfs:label "reference area"@en;
    rdfs:subPropertyOf sdmx-dimension:refArea;
    rdfs:range admingeo:UnitaryAuthority;
    .

eg:averageWeight  a rdf:Property, qb:MeasureProperty;
    rdfs:label "average weight"@en;
    rdfs:subPropertyOf sdmx-measure:obsValue;
    rdfs:range xsd:decimal ;
    .


# -- Observations -----------------------------------------

# Column 1
    
eg:slice1 a qb:Slice;
    qb:sliceStructure      eg:sliceByRegion ;
    eg:refAge              "15"^^xsd:integer ;
    sdmx-dimension:sex     sdmx-code:sex-M ;
    qb:observation         eg:o11, eg:o12, eg:o13, eg:o14 ;
    .

eg:o11 a qb:Observation;
    qb:dataSet             eg:dataset-sapa1 ;
    eg:refArea             ex-geo:romania_00pr ;                  
    eg:averageIMC          32.8066 ;
    .
    
eg:o12 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:france_00pt ;                  
    eg:averageIMC           40.8973 ;
    .

eg:o13 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:germany_00pp ;                  
    eg:averageIMC           41.3367 ;
    .

eg:o14 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:spain_00ph ;
    eg:averageIMC           31.9054 ;
    .

# Column 2
    
eg:slice2 a qb:Slice;
    qb:sliceStructure      eg:sliceByRegion ;
    eg:refAge              "15"^^xsd:integer ;
    sdmx-dimension:sex     sdmx-code:sex-F ;
    qb:observation         eg:o21, eg:o22, eg:o23, eg:o24 ;
    .

eg:o21 a qb:Observation;
    qb:dataSet             eg:dataset-sapa1 ;
    eg:refArea             ex-geo:romania_00pr ;                  
    eg:averageIMC          25.3544 ;
    .
    
eg:o22 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:france_00pt ;                  
    eg:averageIMC           28.7816 ;
    .

eg:o23 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:germany_00pp ;                  
    eg:averageIMC           32.4822 ;
    .

eg:o24 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:spain_00ph ;
    eg:averageIMC           33.0649 ;
    .

# Column 3
    
eg:slice3 a qb:Slice;
    qb:sliceStructure       eg:sliceByRegion ;
    eg:refAge               "55"^^xsd:integer ;
    sdmx-dimension:sex      sdmx-code:sex-M ;
    qb:observation          eg:o31, eg:o32, eg:o33, eg:o34 ;
    .

eg:o31 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:romania_00pr ;                  
    eg:averageIMC           32.6172 ;
    .
    
eg:o32 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:france_00pt ;                  
    eg:averageIMC           35.1057 ;
    .

eg:o33 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:germany_00pp ;                  
    eg:averageIMC           33.4716 ;
    .

eg:o34 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:spain_00ph ;
    eg:averageIMC           35.8641 ;
    .

# Column 4
    
eg:slice4 a qb:Slice;
    qb:sliceStructure       eg:sliceByRegion ;
    eg:refAge               "55"^^xsd:integer ;
    sdmx-dimension:sex      sdmx-code:sex-F ;
    qb:observation          eg:o41, eg:o42, eg:o43, eg:o44 ;
    .

eg:o41 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:romania_00pr ;                  
    eg:averageIMC           34.3885 ;
    .
    
eg:o42 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:france_00pt ;                  
    eg:averageIMC           30.4519 ;
    .

eg:o43 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:germany_00pp ;                  
    eg:averageIMC           34.7685 ;
    .

eg:o44 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:spain_00ph ;
    eg:averageIMC           34.8029 ;
    .

# Column 5
    
eg:slice5 a qb:Slice;
    qb:sliceStructure       eg:sliceByRegion ;
    eg:refAge               "95"^^xsd:integer ;
    sdmx-dimension:sex      sdmx-code:sex-M ;
    qb:observation          eg:o51, eg:o52, eg:o53, eg:o54 ;
    .

eg:o51 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:romania_00pr ;                  
    eg:averageIMC           30.4262 ;
    .
    
eg:o52 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:france_00pt ;                  
    eg:averageIMC           33.4543 ;
    .

eg:o53 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:germany_00pp ;                  
    eg:averageIMC           25.8381 ;
    .

eg:o54 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:spain_00ph ;
    eg:averageIMC           25.8389 ;
    .

# Column 6
    
eg:slice6 a qb:Slice;
    qb:sliceStructure       eg:sliceByRegion ;
    eg:refAge               "95"^^xsd:integer ;
    sdmx-dimension:sex      sdmx-code:sex-M ;
    qb:observation          eg:o61, eg:o62, eg:o63, eg:o64 ;
    .

eg:o61 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:romania_00pr ;                  
    eg:averageIMC           27.1066 ;
    .
    
eg:o62 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:france_00pt ;                  
    eg:averageIMC           39.4865 ;
    .

eg:o63 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:germany_00pp ;                  
    eg:averageIMC           32.1074 ;
    .

eg:o64 a qb:Observation;
    qb:dataSet              eg:dataset-sapa1 ;
    eg:refArea              ex-geo:spain_00ph ;
    eg:averageIMC           32.1491 ;
    .