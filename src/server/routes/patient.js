var express = require('express');
var router = express.Router();
var globals = require("../globals");
var User = require('../models/user');
var util = require('util');
/* Protected routes */
router.get('/doctors', function(req, res, next) {
    globals.getRelatedUsers(req, res);
});

router.get('/diseases', function(req, res, next) {
    if (req.user.extra == undefined || req.user.extra.diseases == undefined) {
        return res.status(404).json({
            success: true,
            message: 'No diseases found!',
            diseases: []
        });
    }
});

router.get('/prescriptions', function(req, res, next) {
    if (req.user.extra == undefined || req.user.extra.recipes == undefined) {
        return res.status(404).json({
            success: true,
            message: 'No recipes found!',
            recipes: []
        });
    }

    var recipes = req.user.extra.recipes;

    return res.status(404).json({
        success: true,
        message: 'Recipes provided!',
        recipes: recipes
    });
});

module.exports = router;