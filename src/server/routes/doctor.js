var express = require('express');
var router = express.Router();
var globals = require("../globals");
var User = require('../models/user');
var util = require("util");

/* Unprotected routes */

router.post('/email_registration', function(req, res, next)     {
    globals.addUser(req, res, true, function(doctor, already_found) {
        if (already_found) {
            return res.status(404).json({
                success: false,
                message: 'User already registered!'
            });
        }

        return res.json({
            success: true,
            message: 'User registered!'
        });
    });
});

/* Protected routes */
router.get('/patients', function(req, res, next) {
    globals.getRelatedUsers(req, res);
});

router.post('/patient/add', function(req, res, next) {
    globals.addUser(req, res, false, function(patient, already_found) {
        var doctor = req.user;
        if (doctor.extra == undefined) {
            doctor.extra = {}
        }
        if (doctor.extra.patients == undefined) {
            doctor.extra.patients = [];
        }

        doctor.extra.patients.push(patient._id);

        if (patient.extra == undefined) {
            patient.extra = {}
        }
        if (patient.extra.doctors == undefined) {
            patient.extra.doctors = [];
        }

        patient.extra.doctors.push(doctor._id);

        doctor.markModified('extra');
        patient.markModified('extra');
        console.log("Doctor:\n" + doctor);
        console.log("Patient:\n" + patient);

        doctor.save(function(err) {
            if (err) throw err;

            console.log('User saved successfully!');
        });
        patient.save(function(err) {
            if (err) throw err;

            console.log('User saved successfully!');
        });

        if (!already_found) {

            res.json({
                success: true,
                message: 'New patient registered!',
                patient: globals.excludeFields(patient)
            });
        }
        else {
            res.json({
                success: true,
                message: 'Patient is already registered!',
                patient: globals.excludeFields(patient)
            });
        }
    });
});

router.post('/patient/disease/add', function(req, res, next) {
    if (req.body.id == undefined || req.body.disease == undefined) {
        return res.status(404).json({
            success: false,
            message: 'Patient ID or disease not provided!'
        });
    }

    User.find({ _id : req.body.id, is_doctor: false }, function(err, users) {
        if (err) throw err;

        if (users.length == 0) {
            return res.status(404).json({
                success: false,
                message: 'Patient ID not found!'
            });
        }

        var user = users[0];
        if (user.extra == undefined) {
            user.extra = {};
        }
        if (user.extra.diseases == undefined) {
            user.extra.diseases = [];
        }

        var path = '/sparql?query=' + encodeURIComponent(util.format(globals.sparqlQuery, req.body.disease.toLowerCase())) + "&format=JSON";
        console.log("Path: ", path);
        globals.getDataFromUrl("dbpedia.org", path, function(data) {
            console.log(data);
            console.log("Disease: ", req.body.disease);

            data = JSON.parse(data);
            var description = "";
            if (data.results.bindings.length > 0) {
                description = data.results.bindings[0].about.value;
            }

            user.extra.diseases.push({
                "title" : req.body.disease,
                "content": description
            });

            user.markModified('extra');
            user.save(function (err) {
                if (err) throw err;

                console.log('User successfully updated!');
            });

            return res.json({
                success: true,
                message: 'Disease added!'
            });
        });


    });
});

router.post('/patient/prescription/add', function(req, res, next) {
    if (req.body.id == undefined || req.body.recipe == undefined) {
        return res.status(404).json({
            success: false,
            message: 'Patient ID not provided!'
        });
    }

    User.find({ _id : req.body.id, is_doctor: false }, function(err, users) {
        if (err) throw err;

        if (len(users) == 0) {
            return res.status(404).json({
                success: false,
                message: 'Patient ID not found!'
            });
        }

        var user = users[0];
        if (user.extra == undefined) {
            user.extra = {};
        }
        if (user.extra.recipes == undefined) {
            user.extra.recipes = [];
        }

        user.extra.recipes.push(req.body.recipe);

        user.markModified('extra');
        user.save(function (err) {
            if (err) throw err;

            console.log('User successfully updated!');
        });

        return res.json({
            success: true,
            message: 'Recipe added!'
        });
    });
});

router.post('/gcm', function(req, res, next) {
    if (req.body.gcm == undefined) {
        return res.status(404).json({
            success: false,
            message: 'Push notification token not provided!'
        });
    }

    if (!req.user.is_doctor) {
        return res.status(404).json({
            success: false,
            message: 'Push notification token can only be set for doctor!'
        });
    }

    user.gcm = req.body.gcm;
    user.save(function (err) {
        if (err) throw err;

        console.log('User successfully updated!');
    });

    return res.json({
        success: true,
        message: 'Push notification token saved!'
    });
});

module.exports = router;
