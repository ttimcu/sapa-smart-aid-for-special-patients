/**
 * Created by ttimcu on 1/30/2017.
 */

var express = require('express');
var router = express.Router();
var globals = require("../globals");
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });
var fs = require('fs');
var User = require('../models/user');
var config = require('../config');


/* Unprotected routes */
router.get('/', function(req, res, next) {
    res.render("index");
});

router.post('/application', function(req, res, next) {
    globals.addUser(req, res, true, function(doctor, already_found) {

    });

    res.render("confirmation");
});

router.post('/register/verify_code', function(req, res, next) {
    if (req.body.code == undefined) {
        return res.status(404).json({
            success: false,
            message: 'No code provided!'
        });
    }

    User.find({ code: req.body.code }, function(err, user) {
        if (err) throw err;

        if (user.length) {
            console.log(user);
            res.json({
                success: true,
                message: 'Code verified!',
                email: user[0].email
            });
        }
        else {
            res.status(404).json({
                success: false,
                message: 'Code not found!'
            });
        }
    });
});

router.post('/register', function(req, res, next) {
    if (req.body.code == undefined || req.body.email == undefined || req.body.password == undefined) {
        return res.status(404).json({
            success: false,
            message: 'Code, email or password was not provided!'
        });
    }

    User.find({ $and: [{code: req.body.code}, {email: req.body.email}] }, function(err, users) {
        if (err) throw err;

        if (users.length) {
            user = users[0];
            if (user.code == undefined) {
                res.status(404).json({
                    success: false,
                    message: 'Already registered!'
                });
            }
            else {
                user.code = undefined;
                user.token = globals.generateUnique(64);
                user.password = req.body.password;
                user.registered = true;
                user.save(function (err) {
                    if (err) throw err;

                    console.log('User successfully updated!');
                });

                console.log(user.token);

                res.json({
                    success: true,
                    message: 'Registered!',

                    token: user.token,
                    email: user.email,
                    is_doctor: user.is_doctor
                });
            }
        }
        else {
            res.status(404).json({
                success: false,
                message: 'Not registered!'
            });
        }
    });
});

router.post('/login', function(req, res, next) {
    if (req.body.email == undefined || req.body.password == undefined) {
        return res.status(404).json({
            success: false,
            message: 'Email or password was not provided!'
        });
    }

    User.find({ $and: [{email : req.body.email}, {password: req.body.password}] }, function(err, users) {
        if (err) throw err;

        if (users.length == 0) {
            return res.status(404).json({
                success: false,
                message: 'Invalid email or password!'
            });
        }

        user = users[0];
        if (user.code != undefined) {
            return res.status(404).json({
                success: false,
                message: 'You have to register first!'
            });
        }

        user.token = globals.generateUnique(64);
        user.save(function(err) {
            if (err) throw err;

            console.log('User successfully updated!');
        });

        return res.json({
            success: true,
            message: 'Logged in!',

            token: user.token,
            is_doctor: user.is_doctor
        });
    });
});

/* Token authentification middleware */
router.use(function(req, res, next) {
    var _ = require('underscore')
        , nonSecurePaths = ['/doctor/email_registration', '/', '/application', '/register/verify_code', '/register', '/login'];

    if ( _.contains(nonSecurePaths, req.path) ) {
        return next();
    }
    else {
        var token_string = req.headers['authorization']; //req.body.token || req.query.token ||
        //console.log(req.headers);
        //console.log(token_string);
        if (token_string) {
            User.find({ token: token_string }, function(err, users) {
                if (err) throw err;

                if (users.length == 0) {
                    return res.status(404).json({
                        success: false,
                        message: 'Invalid token!'
                    });
                }
                user = users[0];

                if (req.path.substring(0, 7) == "/doctor" && !user.is_doctor ||
                    req.path.substring(0, 8) == "/patient" && user.is_doctor) {
                    return res.status(404).json({
                        success: false,
                        message: 'Access denied!'
                    });
                }

                req.user = users[0];
                next();
            });
        } else {
            return res.status(403).send({
                success: false,
                message: 'No token provided!'
            });
        }

    }
});

/* Protected routes */
router.get('/profile', function(req, res, next) {
    res.json(globals.excludeFields(req.user));
});

router.post('/profile', upload.single("photo"), function(req, res, next) {
    console.log(req.body);

    if (req.file) {
        console.log("Got file: ", req.file);
        req.user.photo = "http://" + req.headers.host + "/profile/photo/" + req.file.filename;
    }

    globals.mergeObjects(req.user, req.body);

    console.log(req.user);
    req.user.save(function(err) {
        if (err) throw err;

        console.log('User saved successfully!');
    });

    res.json({
        success: true,
        message: 'User updated successfully!'
    });
});

router.get('/profile/photo/:photo_id', function(req, res, next) {
    var photo_id = req.params.photo_id;
    console.log("Photo ID: ", photo_id);

    var img = fs.readFileSync('./uploads/' + photo_id);
    res.writeHead(200, {'Content-Type': 'image/gif' });
    res.end(img, 'binary');
});

router.get('/extra', function(req, res, next) {
    if (req.user.extra) {
        console.log(req.user.extra);
        return res.json({
            success: true,
            message: 'Got user extra data!',
            extra: req.user.extra
        });
    }
    res.json({
        success: true,
        message: 'No extra data!',
        extra: {}
    });
});

router.post('/extra', function(req, res, next) {
    globals.mergeObjects(req.user.extra, req.body);
    req.user.markModified('extra');
    req.user.save(function (err) {
        if (err) throw err;

        console.log('User successfully updated!');
    });

    if (!req.user.is_doctor) {
        globals.sendPushNotificationAlert(req.user, globals.getPulseStatus, req.user.age, req.body.pulse, "pulse");
        globals.sendPushNotificationAlert(req.user, globals.getTemperatureStatus, req.user.age, req.body.temperature, "temperature");
    }

    res.json({
        success: true,
        message: 'Extra data set!'
    });
});

module.exports = router;