var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config');
var engines = require('consolidate');

var app = express();

var mongoose = require('mongoose');
mongoose.connect(config.database);

//var SparqlClient2 = require('sparql-client-2');
//sparqlClient = new SparqlClient2('http://dbpedia.org/sparql');
    //.register({db: 'http://dbpedia.org/resource/'})
    //.register({dbpedia: 'http://dbpedia.org/property/'});

app.use(express.static(path.join(__dirname, 'public')));

// view engine setup
app.set('views', __dirname + '/views');
app.engine('html', engines.hogan);
app.set('view engine', 'html');

//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/',        require('./routes/common'));
app.use('/doctor',  require('./routes/doctor'));
app.use('/patient', require('./routes/patient'));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  //var err = new Error('Not Found');
  //err.status = 404;
  next();
});

// error handler
app.use(function(err, req, res, next) {
  if (err) {
    throw err;
  }
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(404).json({ status: false, message: "Not found!" });
});

module.exports = app;
