/**
 * Created by ttimcu on 1/30/2017.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    code:      { type: String },
    email:     { type: String, required: true, unique: true },
    password:  { type: String },

    token:     { type: String },
    gcm: { type: String },

    id: String,
    registered: Boolean,
    first_name: String,
    last_name: String,
    phone: String,
    photo: String,

    created_at: Date,
    updated_at: Date,

    is_doctor:  Boolean,
    extra: Schema.Types.Mixed
});

userSchema.pre('save', function(next) {
    var currentDate = new Date();

    this.updated_at = currentDate;

    if (!this.created_at)
        this.created_at = currentDate;

    next();
});

var User = mongoose.model('User', userSchema);
module.exports = User;