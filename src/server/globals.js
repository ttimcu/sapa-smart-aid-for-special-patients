var crypto = require('crypto');
var config = require('./config');
var User = require('./models/user');
var https = require('https');
var UUID = require('uuid/v4');
var gcm = require('node-gcm');
var gcmSender = new gcm.Sender(config.push_notifications_api_key);

var util = require('util');

var nodemailer = require('@nodemailer/pro');
var nodemailerTransporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'theodor.pricop@gmail.com',
        pass: 'theodor.94'
    }
});

module.exports = {
    "generateUnique": function(size) {
        return crypto.randomBytes(size).toString('hex').toUpperCase();
    },
    "mergeObjects": function(obj1, obj2) {
        if (obj1 == undefined || obj2 == undefined) {
            return;
        }

        for (var prop in obj2) {
            console.log(prop, obj2[prop]);
            obj1[prop] = obj2[prop];
        }
    },
    "excludeFields": function(_obj) {
        var obj = JSON.parse(JSON.stringify(_obj));

        obj["id"] = obj._id;
        var f = ["_id", "created_at", "updated_at", "code", "__v", "password", "token", "extra.doctors", "extra.patients"];
        for (var i = 0; i < f.length; i++) {
            //console.log(fields[i]);
            s = f[i].split(".");
            if (s[1] != undefined && obj[s[0]] != undefined) {
                obj[s[0]][s[1]] = undefined;
            }
            else {
                obj[s[0]] = undefined;
            }
        }
        return obj;
    },
    "addUser": function(req, res, isDoctor, cb) {
        if (req.body.email == undefined) {
            res.status(404).json({
                success: false,
                message: 'No email provided!'
            });
            return;
        }

        User.find({ email: req.body.email, is_doctor: isDoctor }, function(err, users) {
            if (err) throw err;

            var newUser = undefined;
            var already_found = false;
            if (users.length != 0) {
                newUser = users[0];
                already_found = true;
            }
            else {
                var code = module.exports.generateUnique(3);
                newUser = new User({
                    code : code,
                    is_doctor : isDoctor,
                    registered : false
                });

                module.exports.mergeObjects(newUser, req.body);

                console.log("Added new user: " + newUser);

                var mailOptions = {
                    from: '"SAPA" <mail@sapa.com>',
                    to: req.body.email,
                    subject: 'SAPA Registration Code',
                    text: 'Hello!\n\nYour SAPA registration code is: ' + code + '\n\nSee you soon,\nSAPA'
                };

                nodemailerTransporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    }
                    console.log('Message %s sent: %s', info.messageId, info.response);
                });

                newUser.save(function(err) {
                    if (err) throw err;

                    console.log('User saved successfully!');
                });
            }

            cb(newUser, already_found);
        });
    },
    "getRelatedUsers": function(req, res) {
        console.log(req.query);
        if (req.query.page == undefined || req.query.count == undefined) {
            return res.status(404).json({
                success: false,
                message: 'Page or count not provided!'
            });
        }

        var user = req.user;
        console.log(user);
        if (user.extra == undefined) {
            return res.json({
                success: true,
                message: 'Retrieved patients!',
                users: []
            });
        }

        var relatedUsersList = user.is_doctor ? user.extra.patients : user.extra.doctors;

        var start_index = req.query.page * req.query.count;
        console.log("Start index: ", start_index, "; Limit: ", parseInt(req.query.count));

        User.find({ _id: { $in: relatedUsersList } })
            .skip(parseInt(start_index))
            .limit(parseInt(req.query.count))
            .sort( { last_name: 1, first_name: 1, email: 1 } )
            .exec(function(err, users) {
                if (err) throw err;

                var finalUsers = users.map(function(item) {
                    return module.exports.excludeFields(item);
                });

                res.json({
                    success: true,
                    message: 'Retrieved users!',
                    users: finalUsers
                });
            });
    },
    "getRelatedUsersForPushNofications" : function(user, cb) {
        var relatedUsersList = user.is_doctor ? user.extra.patients : user.extra.doctors;

        User.find({ _id: { $in: relatedUsersList }, gcm: { $exists: true } }, function(err, users) {
            if (err) throw err;

            var tokens = [];
            for (var i = 0; i < users.length; i++) {
                tokens.push(users[i].gcm);
            }
            console.log("Tokens: ", tokens);
            cb(tokens);
        });
    },
    "getPulseStatus": function(age, pulseValue) {
        if (pulseValue > 110) {
            return "high";
        }
        if (pulseValue < 60) {
            return "low";
        }

        return "normal";
    },
    "getTemperatureStatus": function(age, temperatureValue) {
        if (temperatureValue > 39) {
            return "high";
        }
        if (temperatureValue < 33) {
            return "low";
        }

        return "normal";
    },
    "sendPushNotificationAlert": function(user, status_function, age, value, value_string) {
        if (value == undefined) {
            return;
        }

        var status = status_function(age, parseInt(value));
        console.log("Status: ", status);
        if (status != "normal") {
            module.exports.getRelatedUsersForPushNofications(user, function(tokens) {
                var message = new gcm.Message({
                    priority: 'high',
                    contentAvailable: true,
                    delayWhileIdle: true,
                    notification: {
                        title:   util.format("Patient %s alert", value_string),
                        message: util.format("Patient %s %s has a %s(%s) %s!", user.first_name, user.last_name, status, value, value_string)
                    }
                });

                console.log(message);

                gcmSender.send(message, { registrationTokens: tokens }, function (err, response) {
                    if(err) console.error(err);
                    else 	console.log(response);
                });
            });
        }
    },
    "getDataFromUrl": function(host, path, cb) {
        var options = {
            hostname: host,
            port: 443,
            path: path,
            method: 'GET',
            header: {
                "Accept": "application/json"
            }
        };
        //options.agent = new https.Agent(options);

        var request = https.request(options, function (res) {
            var data = '';
            res.on('data', function (chunk) {
                data += chunk;
            });
            res.on('end', function () {
                console.log(data);
                cb(data);
            });
        });

        request.end();
    },
    "sparqlQuery" :
       "select distinct ?disease ?about \
        where { \
            ?disease rdf:type <http://dbpedia.org/ontology/Disease>. \
            ?disease dbo:abstract ?about . \
            ?disease rdfs:label ?Ilabel . \
            FILTER (langMatches(lang(?about),\"en\")). \
            FILTER (lcase(str(?Ilabel)) = \"%s\") \
        }"

};
