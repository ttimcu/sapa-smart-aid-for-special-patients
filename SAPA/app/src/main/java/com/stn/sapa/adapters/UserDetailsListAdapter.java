package com.stn.sapa.adapters;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.stn.sapa.R;
import com.stn.sapa.models.PrescriptionAndDisease;
import com.stn.sapa.models.User;
import com.stn.sapa.utils.GlideUtils;
import com.stn.sapa.z_base.BaseExpandableRecyclerViewAdapter;
import com.stn.sapa.z_base.ViewsHolder;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Sebastian on 2/1/2017.
 */

public class UserDetailsListAdapter extends BaseExpandableRecyclerViewAdapter<PrescriptionAndDisease, String> {

    private User user;

    public UserDetailsListAdapter(Context context, User user, ArrayList<PrescriptionAndDisease> parents) {
        super(context, parents);
        this.user = user;
    }

    @Override
    protected int getHeaderLayoutId() {
        return R.layout.user_details_list_header;
    }

    @Override
    protected void setHeaderData(ViewsHolder views) {
        super.setHeaderData(views);

        Glide.with(context).load(GlideUtils.getGlideUrl(user.getPhoto())).bitmapTransform(new CropCircleTransformation(context)).into(views.getImageView(R.id.iv_user_details_activity_photo));
        views.getTextView(R.id.tv_user_details_activity_name).setText(user.getFirst_name() + " " + user.getLast_name());
        views.getTextView(R.id.tv_user_details_activity_email).setText(user.getEmail());
        views.getTextView(R.id.tv_user_details_activity_phone).setText(user.getPhone());
    }

    @Override
    protected int getParentLayoutId() {
        return R.layout.user_details_list_parent_item;
    }

    @Override
    protected int getChildLayoutId() {
        return R.layout.user_details_list_child_item;
    }

    @Override
    protected ArrayList<String> getChildrenFromParent(PrescriptionAndDisease parent) {
        ArrayList<String> children = new ArrayList<>();
        children.add(parent.getContent());
        return children;
    }

    @Override
    protected void setParentData(ViewsHolder views, PrescriptionAndDisease item, boolean isExpanded, int position) {
        views.getImageView(R.id.iv_user_details_list_parent_item_icon).setImageResource(item.getResourceId());
        views.getTextView(R.id.tv_user_details_list_parent_item_title).setText(item.getTitle());
    }

    @Override
    protected void setParentActions(ViewsHolder views, PrescriptionAndDisease item, boolean isExpanded, int position) {
    }

    @Override
    protected void setChildData(ViewsHolder views, String item, int position) {
        views.getTextView(R.id.tv_user_details_list_child_item).setText(item);
    }

    @Override
    protected void setChildActions(ViewsHolder views, String item, int position) {

    }
}
