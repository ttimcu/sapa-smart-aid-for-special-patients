package com.stn.sapa.utils;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.stn.sapa.managers.AppInfoManager;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class GlideUtils {

    public static GlideUrl getGlideUrl(String url) {
        return new GlideUrl(url, new LazyHeaders.Builder().addHeader("Authorization", AppInfoManager.getInstance().getAuthorizationToken()).build());
    }
}
