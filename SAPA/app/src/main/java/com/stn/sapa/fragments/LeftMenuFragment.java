package com.stn.sapa.fragments;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stn.sapa.R;
import com.stn.sapa.activities.MainActivity;
import com.stn.sapa.activities.ProfileActivity;
import com.stn.sapa.managers.AppInfoManager;
import com.stn.sapa.models.UserProfile;
import com.stn.sapa.utils.GlideUtils;
import com.stn.sapa.z_base.BaseFragment;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class LeftMenuFragment extends BaseFragment {

    private TextView tv_left_menu_fragment_name;
    private Button b_left_menu_fragment_edit;
    private ImageView iv_left_menu_fragment_photo;

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.left_menu_fragment;
    }

    @Override
    protected void linkUI() {
        tv_left_menu_fragment_name = views.getTextView(R.id.tv_left_menu_fragment_name);
        b_left_menu_fragment_edit = views.getButton(R.id.b_left_menu_fragment_edit);

        iv_left_menu_fragment_photo = views.getImageView(R.id.iv_left_menu_fragment_photo);
    }

    @Override
    protected void init() {
        UserProfile userProfile = AppInfoManager.getInstance().getUserProfile();
        tv_left_menu_fragment_name.setText(userProfile.getFirst_name() + " " + userProfile.getLast_name());

        if (userProfile.getPhoto() != null)
            Glide.with(getActivity()).load(GlideUtils.getGlideUrl(userProfile.getPhoto())).bitmapTransform(new CropCircleTransformation(getActivity())).into(iv_left_menu_fragment_photo);
    }

    @Override
    protected void setData() {

    }

    @Override
    protected void setActions() {
        b_left_menu_fragment_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileActivity.startActivityWithProfilePhotoTransition(getActivity(), iv_left_menu_fragment_photo);
            }
        });

        views.get(R.id.b_left_menu_fragment_log_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });

        views.get(R.id.ll_left_menu_fragment_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=home");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    getActivity().startActivity(mapIntent);
                }
            }
        });
    }
}
