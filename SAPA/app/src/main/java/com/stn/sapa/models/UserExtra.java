package com.stn.sapa.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Sebastian on 2/1/2017.
 */

public class UserExtra implements Serializable {

    private String temperature;
    private String pulse;
    private String latitude;
    private String longitude;
    private ArrayList<PrescriptionAndDisease> diseases;
    private ArrayList<PrescriptionAndDisease> prescriptions;

    public String getTemperature() {
        return temperature;
    }

    public String getPulse() {
        return pulse;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public ArrayList<PrescriptionAndDisease> getDiseases() {
        return diseases;
    }

    public ArrayList<PrescriptionAndDisease> getPrescriptions() {
        return prescriptions;
    }
}
