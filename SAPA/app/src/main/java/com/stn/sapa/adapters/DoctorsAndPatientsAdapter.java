package com.stn.sapa.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.stn.sapa.R;
import com.stn.sapa.activities.UserDetailsActivity;
import com.stn.sapa.models.User;
import com.stn.sapa.utils.GlideUtils;
import com.stn.sapa.z_base.BaseRecyclerViewAdapter;
import com.stn.sapa.z_base.ViewsHolder;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class DoctorsAndPatientsAdapter extends BaseRecyclerViewAdapter<User> {

    private RequestManager glideRequestManager;

    public DoctorsAndPatientsAdapter(Context context, ArrayList<User> items) {
        super(context, items);

        this.glideRequestManager = Glide.with(context);
    }

    @Override
    protected int getItemLayoutId() {
        return R.layout.patient_item;
    }

    @Override
    protected void setData(final ViewsHolder views, final User item, int pos) {
        if (item.isRegistered()) {
            views.get(R.id.iv_patient_item_photo_registered).setVisibility(View.VISIBLE);
            views.get(R.id.iv_patient_item_photo_unregistered).setVisibility(View.GONE);
            views.get(R.id.tv_patient_item_name).setVisibility(View.VISIBLE);
            views.get(R.id.tv_patient_item_phone).setVisibility(View.VISIBLE);
            views.get(R.id.cv_patient_item).setAlpha(1.0f);

            glideRequestManager.load(GlideUtils.getGlideUrl(item.getPhoto())).bitmapTransform(new CropCircleTransformation(context)).into(views.getImageView(R.id.iv_patient_item_photo_registered));

            views.getTextView(R.id.tv_patient_item_name).setText(item.getFirst_name() + " " + item.getLast_name());
            views.getTextView(R.id.tv_patient_item_email).setText(item.getEmail());
            views.getTextView(R.id.tv_patient_item_phone).setText(item.getPhone());

            views.get(R.id.fl_patient_item_click).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UserDetailsActivity.startActivityWithSharedElements((Activity) context
                            , item
                            , views.getImageView(R.id.iv_patient_item_photo_registered)
                            , views.getTextView(R.id.tv_patient_item_name)
                            , views.getTextView(R.id.tv_patient_item_email)
                            , views.getTextView(R.id.tv_patient_item_phone));
                }
            });
        } else {
            views.get(R.id.iv_patient_item_photo_registered).setVisibility(View.GONE);
            views.get(R.id.iv_patient_item_photo_unregistered).setVisibility(View.VISIBLE);
            views.get(R.id.tv_patient_item_name).setVisibility(View.INVISIBLE);
            views.get(R.id.tv_patient_item_phone).setVisibility(View.INVISIBLE);
            views.get(R.id.cv_patient_item).setAlpha(0.5f);

            views.getTextView(R.id.tv_patient_item_email).setText(item.getEmail());
        }
    }
}
