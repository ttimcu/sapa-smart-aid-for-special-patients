package com.stn.sapa.retrofit.requestbodies;

/**
 * Created by Sebastian on 2/1/2017.
 */

public class DoctorGCMTokenBody {

    private String gcm;

    public DoctorGCMTokenBody(String gcm) {
        this.gcm = gcm;
    }
}
