package com.stn.sapa.managers;

import com.stn.sapa.models.UserProfile;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class AppInfoManager {

    private static AppInfoManager INSTANCE;

    private String authorizationToken;
    private UserProfile userProfile;

    public static AppInfoManager getInstance() {
        if (INSTANCE == null)
            INSTANCE = new AppInfoManager();

        return INSTANCE;
    }

    private AppInfoManager() {

    }

    public String getAuthorizationToken() {
        return authorizationToken;
    }

    public void setAuthorizationToken(String authorizationToken) {
        this.authorizationToken = authorizationToken;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
