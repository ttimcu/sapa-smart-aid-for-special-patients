package com.stn.sapa.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import com.stn.sapa.R;


/**
 * Created by Sebastian on 8/12/2016.
 */
public class DialogUtils {

    private static DialogUtils instance;
    private static Activity currentActivity;

    private ProgressDialog progressDialog;
    private int currentProgressDialogMessage;

    private AlertDialog errorDialog;
    private int currentErrorDialogMessage;

    public static DialogUtils getInstance(Activity activity) {
        if (instance == null || !currentActivity.getLocalClassName().contentEquals(activity.getLocalClassName()) || currentActivity.isFinishing()) {
            currentActivity = activity;
            instance = new DialogUtils();
        }

        return instance;
    }

    private DialogUtils() {
        progressDialog = new ProgressDialog(currentActivity);
        progressDialog.setTitle(R.string.please_wait);
        progressDialog.setCancelable(false);

        errorDialog = new AlertDialog.Builder(currentActivity)
                .setTitle(R.string.error)
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }

    public void showProgressDialog(@StringRes int messageStringRes) {
        if (currentProgressDialogMessage != messageStringRes) {
            currentProgressDialogMessage = messageStringRes;
            progressDialog.setMessage(currentActivity.getString(messageStringRes));
        }

        progressDialog.show();
    }

    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    public void showErrorDialog(@StringRes int messageStringRes) {
        if (currentErrorDialogMessage != messageStringRes) {
            currentErrorDialogMessage = messageStringRes;

            errorDialog.setMessage(currentActivity.getString(messageStringRes));
        }

        errorDialog.show();
    }
}
