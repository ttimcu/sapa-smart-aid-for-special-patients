package com.stn.sapa.z_base;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Created by Sebi Ursulescu on 7/5/2015.
 */
public class ViewsHolder extends RecyclerView.ViewHolder {

    private SparseArray<View> views;
    private SparseArray<View> otherViews;
    public View root;

    private InputMethodManager imm;

    /**
     * @param root The root view from where you want to take the views
     */
    public ViewsHolder(View root) {
        super(root);
        views = new SparseArray<>();
        otherViews = new SparseArray<>();

        this.root = root;
        this.imm = (InputMethodManager) root.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        linkViews(root);
    }

    private void linkViews(View view) {
        if (view.getId() != View.NO_ID)
            otherViews.put(view.getId(), view);

        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;

            for (int i = 0; i < group.getChildCount(); i++)
                linkViews(group.getChildAt(i));
        }

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getContext() instanceof Activity) {
                    View currentFocusedView = ((Activity) view.getContext()).getCurrentFocus();
                    if (currentFocusedView != null) {
                        imm.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), 0);
                        view.requestFocus();
                    }
                }
                return false;
            }
        });
    }

    /**
     * @param resId The id of the View you want to be returned
     * @return The View corresponding to the id passe as a parameter
     */
    public View get(int resId) {
        View view = views.get(resId);
        if (view == null) {
            view = otherViews.get(resId);

            if (view != null)
                views.put(resId, view);

            return view;
        } else {
            return view;
        }
    }

    /**
     * @param resId The id of the TextView you want to be returned
     * @return The TextView corresponding to the id passe as a parameter
     */
    public TextView getTextView(int resId) {
        return (TextView) get(resId);
    }

    /**
     * @param resId The id of the EditText you want to be returned
     * @return The EditText corresponding to the id passe as a parameter
     */
    public EditText getEditText(int resId) {
        return (EditText) get(resId);
    }

    /**
     * @param resId The id of the ImageView you want to be returned
     * @return The ImageView corresponding to the id passe as a parameter
     */
    public ImageView getImageView(int resId) {
        return (ImageView) get(resId);
    }

    /**
     * @param resId The id of the RecyclerView you want to be returned
     * @return The RecyclerView corresponding to the id passe as a parameter
     */
    public RecyclerView getRecyclerView(int resId) {
        return (RecyclerView) get(resId);
    }

    /**
     * @param resId The id of the WebView you want to be returned
     * @return The WebView corresponding to the id passe as a parameter
     */
    public WebView getWebView(int resId) {
        return (WebView) get(resId);
    }

    /**
     * @param resId The id of the ScrollView you want to be returned
     * @return The ScrollView corresponding to the id passe as a parameter
     */
    public ScrollView getScrollView(int resId) {
        return (ScrollView) get(resId);
    }

    /**
     * @param resId The id of the LinearLayout you want to be returned
     * @return The LinearLayout corresponding to the id passe as a parameter
     */
    public LinearLayout getLinearLayout(int resId) {
        return (LinearLayout) get(resId);
    }

    /**
     * @param resId The id of the RelativeLayout you want to be returned
     * @return The RelativeLayout corresponding to the id passe as a parameter
     */
    public RelativeLayout getRelativeLayout(int resId) {
        return (RelativeLayout) get(resId);
    }

    /**
     * @param resId The id of the FrameLayout you want to be returned
     * @return The FrameLayout corresponding to the id passe as a parameter
     */
    public FrameLayout getFrameLayout(int resId) {
        return (FrameLayout) get(resId);
    }

    /**
     * @param resId The id of the Button you want to be returned
     * @return The Button corresponding to the id passe as a parameter
     */
    public Button getButton(int resId) {
        return (Button) get(resId);
    }
}
