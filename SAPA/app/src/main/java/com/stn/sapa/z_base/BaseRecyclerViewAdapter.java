package com.stn.sapa.z_base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Sebastian on 9/3/2015.
 */
public abstract class BaseRecyclerViewAdapter<T> extends BaseRecyclerViewHeaderAndFooterAdapter {

    protected ArrayList<T> items;
    private OnItemClickListener listener;

    public BaseRecyclerViewAdapter(Context context, ArrayList<T> items) {
        super(context);
        this.items = items;
    }

    @Override
    protected ViewsHolder onViewHolderCreate(ViewGroup parent, int viewType) {
        return new ViewsHolder(inflater.inflate(getItemLayoutId(), parent, false));
    }

    @Override
    protected void onViewHolderBind(ViewsHolder views, final int position) {
        setData(views, items.get(position), position);
        setActions(views, items.get(position), position);
        views.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getTotalItemCount() {
        return items.size();
    }


    protected abstract int getItemLayoutId();

    /**
     * Implement to populate or set up views with existing data.
     *
     * @param views ViewsHolder for accessing the views in this row layout.
     * @param item  Item corresponding to this row.
     * @param pos   Position of this row.
     */
    protected abstract void setData(ViewsHolder views, T item, int pos);

    /**
     * Override to add callbacks to actions/events.
     *
     * @param views ViewsHolder for accessing the views in this row layout.
     * @param item  Item corresponding to this row.
     * @param pos   Position of this row.
     */
    protected void setActions(ViewsHolder views, final T item, final int pos) {

    }

    /**
     * Interface definition for a callback to be invoked when a view is clicked.
     */
    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param listener The listener that is called when the view is clicked
     */
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
