package com.stn.sapa.retrofit.requestbodies;

/**
 * Created by Sebastian on 1/30/2017.
 */

public class EmailRegistrationBody {

    private String email;

    public EmailRegistrationBody(String email) {
        this.email = email;
    }
}
