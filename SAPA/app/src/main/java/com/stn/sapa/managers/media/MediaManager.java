package com.stn.sapa.managers.media;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.stn.sapa.R;

import java.io.File;

/**
 * Created by Sebastian on 7/9/2016.
 */
public class MediaManager {

    private static MediaManager instance;
    private static Activity currenctActivity;

    private AlertDialog choosePhotoRetrievalMethodDialog;

    private GalleryManager galleryManager;
    private CameraManager cameraManager;

    public static MediaManager getInstance(Activity newActivity) {
        if (instance == null || !currenctActivity.getLocalClassName().contentEquals(newActivity.getLocalClassName()) || currenctActivity.isFinishing()) {
            currenctActivity = newActivity;
            instance = new MediaManager();
        }

        return instance;
    }

    private MediaManager() {
        galleryManager = new GalleryManager(currenctActivity);
        cameraManager = new CameraManager(currenctActivity);
    }

    public void getPhoto(final OnMediaItemReceivedCallback<File> callback) {
        if (choosePhotoRetrievalMethodDialog == null) {
            choosePhotoRetrievalMethodDialog = new AlertDialog.Builder(currenctActivity).setTitle(R.string.photo).setNegativeButton(R.string.gallery, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    browsePhoto(callback);
                }
            }).setPositiveButton(R.string.camera, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    capturePhoto(callback);
                }
            }).create();
        }

        choosePhotoRetrievalMethodDialog.show();
    }

    private void browsePhoto(final OnMediaItemReceivedCallback<File> callback) {
        galleryManager.openPhotoGallery(callback);
    }

    private void capturePhoto(final OnMediaItemReceivedCallback<File> callback) {
        cameraManager.capturePhoto(callback);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        galleryManager.onActivityResult(requestCode, resultCode, data);
        cameraManager.onActivityResult(requestCode, resultCode);
    }

    public interface OnMediaItemReceivedCallback<T> {
        void onMediaItemReceived(T mediaItem);
    }
}
