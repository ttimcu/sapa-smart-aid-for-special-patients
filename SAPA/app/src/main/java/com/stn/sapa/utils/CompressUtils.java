package com.stn.sapa.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.stn.sapa.R;

import java.io.File;
import java.io.IOException;

/**
 * Created by Sebastian on 7/9/2016.
 */
public class CompressUtils {

    private static final int maximumWidth = 1280;
    private static final int maximumHeight = 720;

    public static void compressPhoto(final Activity activity, final Uri photoToCompressUri, final OnCompressionFinishedCallback<File> callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogUtils.getInstance(activity).showProgressDialog(R.string.compressing_photo);
                        }
                    });

                    Bitmap bitmapToCompress = BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(photoToCompressUri));

                    int bitmapScaledWidth = bitmapToCompress.getWidth();
                    int bitmapScaledHeight = bitmapToCompress.getHeight();

                    if (bitmapScaledWidth > maximumWidth) {
                        bitmapScaledHeight -= ((float) bitmapScaledHeight / bitmapScaledWidth) * (bitmapScaledWidth - maximumWidth);
                        bitmapScaledWidth = maximumWidth;
                    }

                    if (bitmapScaledHeight > maximumHeight) {
                        bitmapScaledWidth -= ((float) bitmapScaledWidth / bitmapScaledHeight) * (bitmapScaledHeight - maximumHeight);
                        bitmapScaledHeight = maximumHeight;
                    }

                    if (bitmapScaledWidth < bitmapToCompress.getWidth() || bitmapScaledHeight < bitmapToCompress.getHeight())
                        bitmapToCompress = Bitmap.createScaledBitmap(bitmapToCompress, bitmapScaledWidth, bitmapScaledHeight, false);

                    final File compressedFile = MediaFileUtils.createMediaPhotoTempFile(activity);

                    MediaFileUtils.savePhoto(bitmapToCompress, compressedFile);

                    if (callback != null)
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.getInstance(activity).dismissProgressDialog();
                                callback.onCompressionFinished(compressedFile);
                            }
                        });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public interface OnCompressionFinishedCallback<T> {
        void onCompressionFinished(T compressedObject);
    }
}
