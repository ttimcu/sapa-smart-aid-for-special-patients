package com.stn.sapa.models;

import java.io.Serializable;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class UserProfile implements Serializable {

    private String photo;
    private String first_name;
    private String last_name;
    private String phone;
    private boolean is_doctor;

    public String getPhoto() {
        return photo;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPhone() {
        return phone;
    }

    public boolean is_doctor() {
        return is_doctor;
    }

}
