package com.stn.sapa.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Sebastian on 7/21/2016.
 */
public class MediaFileUtils {

    public static final String EXTENSION_PHOTO = ".jpg";

    public static File createMediaPhotoTempFile(Context context) throws IOException {
        String imageFileName = "JPEG_" + new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS").format(new Date()) + "_";
        return createMediaPhotoFile(context, "", imageFileName);
    }

    private static File createMediaPhotoFile(Context context, String directoryFromPictures, String fileName) throws IOException {
        File storageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath() + directoryFromPictures);

        storageDir.mkdir();

        return createMediaFile(storageDir, fileName, EXTENSION_PHOTO);
    }

    private static File createMediaFile(File directory, String fileName, String extension) {
        return new File(directory, fileName + extension);
    }

    static void savePhoto(Bitmap photoBitmap, File savedPhotoFile) throws FileNotFoundException {
        photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(savedPhotoFile));
    }
}
