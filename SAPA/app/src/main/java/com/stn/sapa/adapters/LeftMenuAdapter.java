package com.stn.sapa.adapters;

import android.content.Context;

import com.stn.sapa.R;
import com.stn.sapa.z_base.BaseRecyclerViewAdapter;
import com.stn.sapa.z_base.ViewsHolder;

import java.util.ArrayList;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class LeftMenuAdapter extends BaseRecyclerViewAdapter<String> {
    public LeftMenuAdapter(Context context, ArrayList<String> items) {
        super(context, items);
    }

    @Override
    protected int getItemLayoutId() {
        return R.layout.left_menu_item;
    }

    @Override
    protected void setData(ViewsHolder views, String item, int pos) {
        views.getTextView(R.id.tv_left_menu_item).setText(item);
    }
}
