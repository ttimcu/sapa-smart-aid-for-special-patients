package com.stn.sapa.z_base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Sebastian on 9/3/2015.
 */
public abstract class BaseExpandableRecyclerViewAdapter<P, C> extends BaseRecyclerViewHeaderAndFooterAdapter {

    private static final int PARENT = 1;
    private static final int CHILD = 2;

    protected ArrayList<P> parents;
    protected ArrayList<ArrayList<C>> children;

    private ArrayList<Object> items;

    private int parentOpened = -1;

    /**
     * @return The layout of the parents in the expandable list
     */
    protected abstract int getParentLayoutId();

    /**
     * @return The layout of the children in the expandable list
     */
    protected abstract int getChildLayoutId();

    /**
     * @return The id of the view which will be clicked to expand the parent
     */
    protected int getOpenChildrenOnClickViewId() {
        return 0;
    }

    /**
     * @param parent The current parent item and info
     * @return The {@link ArrayList ArrayList} of children that parent has
     */
    protected abstract ArrayList<C> getChildrenFromParent(P parent);

    /**
     * The constructor of the BaseExpandableRecyclerViewAapter
     *
     * @param context The current context you are in
     * @param parents The {@link ArrayList ArrayList} of parents for the list
     */
    public BaseExpandableRecyclerViewAdapter(Context context, ArrayList<P> parents) {
        super(context);
        this.parents = parents;

        this.children = new ArrayList<>();
        this.items = new ArrayList<>();

        for (P parent : parents) {
            children.add(getChildrenFromParent(parent));
            items.add(parent);
        }
    }

    @Override
    protected ViewsHolder onViewHolderCreate(ViewGroup parent, int viewType) {
        View view;

        if (viewType == PARENT)
            view = inflater.inflate(getParentLayoutId(), parent, false);
        else
            view = inflater.inflate(getChildLayoutId(), parent, false);

        return new ViewsHolder(view);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onViewHolderBind(final ViewsHolder views, final int position) {
        if (views.getItemViewType() == PARENT) {
            View onClickOpenView = views.root;

            if (getOpenChildrenOnClickViewId() != 0)
                onClickOpenView = views.get(getOpenChildrenOnClickViewId());

            onClickOpenView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int truePosition = getAdapterPosition(views);
                    if (!onParentClicked(parentOpened >= truePosition || parentOpened == -1 ? truePosition : truePosition - 1))
                        if (parentOpened == -1) {
                            parentOpened = getAdapterPosition(views);

                            items.addAll(parentOpened + 1, children.get(parentOpened));

                            notifyItemRangeWasInserted(parentOpened + 1, children.get(parentOpened).size());
                            notifyItemWasChanged(parentOpened);
                        } else if (parentOpened == getAdapterPosition(views)) {
                            for (int i = 0; i < children.get(parentOpened).size(); i++)
                                items.remove(parentOpened + 1);

                            notifyItemRangeWasRemoved(parentOpened + 1, children.get(parentOpened).size());
                            notifyItemWasChanged(parentOpened);

                            parentOpened = -1;
                        } else {
                            for (int i = 0; i < children.get(parentOpened).size(); i++)
                                items.remove(parentOpened + 1);

                            notifyItemRangeWasRemoved(parentOpened + 1, children.get(parentOpened).size());
                            notifyItemWasChanged(parentOpened);

                            parentOpened = getAdapterPosition(views);

                            items.addAll(parentOpened + 1, children.get(parentOpened));

                            notifyItemRangeWasInserted(parentOpened + 1, children.get(parentOpened).size());
                            notifyItemWasChanged(parentOpened);
                        }
                }
            });

            boolean isExpanded = false;
            if (parentOpened == getAdapterPosition(views))
                isExpanded = true;

            setParentData(views, (P) items.get(position), isExpanded, position);
            setParentActions(views, (P) items.get(position), isExpanded, position);
        } else {
            setChildData(views, (C) items.get(position), position);
            setChildActions(views, (C) items.get(position), position);
        }
    }

    /**
     * The method where you can set all the data in each parent item
     *
     * @param views      The {@link ViewsHolder ViewsHolder} of the parent item with all the items in the parent layout returned by {@link #getParentLayoutId() getParentLayoutId}
     * @param item       The {@link P parent} item with all the info
     * @param isExpanded A boolean that tells you if the parent is expanded or not
     * @param position   The position of the current parent item
     */
    protected abstract void setParentData(ViewsHolder views, P item, boolean isExpanded, int position);

    /**
     * The method where you can set all the functionalities in each parent item
     *
     * @param views      The {@link ViewsHolder ViewsHolder} of the parent item with all the items in the parent layout returned by {@link #getParentLayoutId() getParentLayoutId}
     * @param item       The {@link P parent} item with all the info
     * @param isExpanded A boolean that tells you if the parent is expanded or not
     * @param position   The position of the current parent item
     */
    protected abstract void setParentActions(ViewsHolder views, P item, boolean isExpanded, int position);

    /**
     * The method where you can set all the data in each child item
     *
     * @param views    The {@link ViewsHolder ViewsHolder} of the child item with all the items in the child layout returned by {@link #getChildLayoutId() getChildLayoutId}
     * @param item     The {@link P parent} item with all the info
     * @param position The position of the current parent item
     */
    protected abstract void setChildData(ViewsHolder views, C item, int position);

    /**
     * The method where you can set all the functionalities in each child item
     *
     * @param views    The {@link ViewsHolder ViewsHolder} of the child item with all the items in the child layout returned by {@link #getChildLayoutId() getChildLayoutId}
     * @param item     The {@link P parent} item with all the info
     * @param position The position of the current parent item
     */
    protected abstract void setChildActions(ViewsHolder views, C item, int position);

    protected boolean onParentClicked(int pos) {
        return false;
    }

    @Override
    public int getViewType(int position) {
        if (parentOpened == -1)
            return PARENT;
        else if (position <= parentOpened || position > parentOpened + children.get(parentOpened).size())
            return PARENT;
        else
            return CHILD;
    }

    @Override
    public int getTotalItemCount() {
        return items.size();
    }
}
