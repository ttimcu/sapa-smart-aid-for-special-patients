package com.stn.sapa.retrofit.general;

import android.app.Activity;

import com.stn.sapa.managers.AppInfoManager;
import com.stn.sapa.models.UserProfile;
import com.stn.sapa.retrofit.requestbodies.DoctorGCMTokenBody;
import com.stn.sapa.retrofit.requestbodies.EmailRegistrationBody;
import com.stn.sapa.retrofit.requestbodies.ExtraBody;
import com.stn.sapa.retrofit.requestbodies.UserLoginBody;
import com.stn.sapa.retrofit.requestbodies.UserRegisterBody;
import com.stn.sapa.retrofit.requestbodies.UserRegistrationVerifyCodeBody;
import com.stn.sapa.retrofit.responsebodies.AddPatientResponse;
import com.stn.sapa.retrofit.responsebodies.DoctorsAndPatientsResponse;
import com.stn.sapa.retrofit.responsebodies.DoctorVerifyCodeResponse;
import com.stn.sapa.retrofit.responsebodies.LoginAndRegisterResponse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sebastian on 1/29/2017.
 */

public class RetrofitUtils {

    public static final int PAGINATION_COUNT = 20;

    private static RetrofitUtils INSTANCE;

    private HashMap<String, ArrayList<Call>> pendingCallsMap;
    private API api;

    public static RetrofitUtils getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RetrofitUtils();
        }

        return INSTANCE;
    }

    private RetrofitUtils() {
        api = new Retrofit.Builder().baseUrl(API.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .addNetworkInterceptor(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request request = chain.request();

                                if (AppInfoManager.getInstance().getAuthorizationToken() != null)
                                    request = request.newBuilder().addHeader("Authorization", AppInfoManager.getInstance().getAuthorizationToken()).build();

                                return chain.proceed(request);
                            }
                        })
                        .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build())
                .build().create(API.class);

        pendingCallsMap = new HashMap<>();
    }

    public void registerDoctorEmail(Activity activity, String email, SapaRetrofitCallback<Void> callback) {
        addCallToMap(activity, api.registerDoctorEmail(new EmailRegistrationBody(email))).enqueue(callback);
    }

    public void verifyRegistrationCode(Activity activity, String code, SapaRetrofitCallback<DoctorVerifyCodeResponse> callback) {
        addCallToMap(activity, api.verifyRegistrationCode(new UserRegistrationVerifyCodeBody(code))).enqueue(callback);
    }

    public void registerUser(Activity activity, String code, String email, String password, SapaRetrofitCallback<LoginAndRegisterResponse> callback) {
        addCallToMap(activity, api.registerUser(new UserRegisterBody(code, email, password))).enqueue(callback);
    }

    public void loginUser(Activity activity, String email, String password, SapaRetrofitCallback<LoginAndRegisterResponse> callback) {
        addCallToMap(activity, api.loginUser(new UserLoginBody(email, password))).enqueue(callback);
    }

    public void saveProfile(Activity activity, File profilePhoto, String firstName, String lastName, String phone, SapaRetrofitCallback<Void> callback) {
        HashMap<String, RequestBody> partMap = new HashMap<>();

        partMap.put("first_name", createRequestBodyFromString(firstName));
        partMap.put("last_name", createRequestBodyFromString(lastName));
        partMap.put("phone", createRequestBodyFromString(phone));

        addCallToMap(activity, api.saveProfile(partMap, MultipartBody.Part.createFormData("photo", profilePhoto.getName(), createRequestBodyFromFile(profilePhoto)))).enqueue(callback);
    }

    public void getProfile(Activity activity, SapaRetrofitCallback<UserProfile> callback) {
        addCallToMap(activity, api.getProfile()).enqueue(callback);
    }

    public void addNewPatient(Activity activity, String email, SapaRetrofitCallback<AddPatientResponse> callback) {
        addCallToMap(activity, api.addNewPatient(new EmailRegistrationBody(email))).enqueue(callback);
    }

    public void getPatients(Activity activity, int page, SapaRetrofitCallback<DoctorsAndPatientsResponse> callback) {
        addCallToMap(activity, api.getPatients(page, PAGINATION_COUNT)).enqueue(callback);
    }

    public void sendGCMToken(String token, SapaRetrofitCallback<Void> callback) {
        api.sendGcmToken(new DoctorGCMTokenBody(token)).enqueue(callback);
    }

    public void sendTempPulseAndLocation(Activity activity, int pulse, float temp, double latitude, double longitude, SapaRetrofitCallback<Void> callback) {
        addCallToMap(activity, api.sendTempAndPulse(new ExtraBody(String.valueOf(pulse), String.valueOf(temp), String.valueOf(latitude), String.valueOf(longitude)))).enqueue(callback);
    }

    public void getDoctors(Activity activity, int page, SapaRetrofitCallback<DoctorsAndPatientsResponse> callback) {
        addCallToMap(activity, api.getDoctors(page, PAGINATION_COUNT)).enqueue(callback);
    }

    private RequestBody createRequestBodyFromString(String info) {
        return RequestBody.create(MultipartBody.FORM, info);
    }

    private RequestBody createRequestBodyFromFile(File file) {
        return RequestBody.create(MediaType.parse("image/jpeg"), file);
    }

    private <T> Call<T> addCallToMap(Activity activity, Call<T> call) {
        String key = activity.getLocalClassName();

        ArrayList<Call> calls = pendingCallsMap.get(key);

        if (calls == null)
            calls = new ArrayList<>();

        calls.add(call);

        pendingCallsMap.put(key, calls);

        return call;
    }

    void removePendingCallFromMap(Call call) {
        for (Map.Entry<String, ArrayList<Call>> activityEntry : pendingCallsMap.entrySet())
            if (activityEntry.getValue().remove(call))
                break;
    }

    public void cancelActivityPendingRequests(Activity activity) {
        String key = activity.getLocalClassName();

        if (pendingCallsMap.containsKey(key)) {
            ArrayList<Call> calls = pendingCallsMap.get(key);

            for (int i = calls.size() - 1; i >= 0; i--) {
                calls.get(i).cancel();
                calls.remove(i);
            }
        }
    }
}
