package com.stn.sapa.z_base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.stn.sapa.managers.LocationManager;
import com.stn.sapa.managers.PermissionsManager;
import com.stn.sapa.managers.media.MediaManager;
import com.stn.sapa.retrofit.general.RetrofitUtils;

/**
 * Created by Sebi Ursulescu on 7/5/2015.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected ViewsHolder views;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = getLayoutInflater().inflate(getActivityLayoutId(), null);

        doStuffBeforeSetContentView();

        setContentView(rootView);

        views = new ViewsHolder(rootView);

        doStuffBeforeLinkUI();

        linkUI();
        init();
        setData();
        setActions();
    }

    protected void doStuffBeforeSetContentView() {

    }

    protected abstract int getActivityLayoutId();

    protected void doStuffBeforeLinkUI() {

    }

    protected abstract void linkUI();

    protected abstract void init();

    protected abstract void setData();

    protected abstract void setActions();

    @Override
    protected void onResume() {
        super.onResume();

        LocationManager.getInstance(this).connect();

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocationManager.getInstance(this).disconnect();
        if (isFinishing())
            RetrofitUtils.getInstance().cancelActivityPendingRequests(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        MediaManager.getInstance(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LocationManager.getInstance(this).onRequestPermissionsResult(requestCode, grantResults);
        PermissionsManager.getInstance(this).onRequestPermissionsResult(requestCode, grantResults);
    }

}
