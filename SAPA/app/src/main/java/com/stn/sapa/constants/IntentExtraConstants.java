package com.stn.sapa.constants;

/**
 * Created by Sebastian on 9/21/2016.
 */
public class IntentExtraConstants {

    public static final String EXTRA_REVEAL_VIEW_SOURCE_CENTER_X = "reveal_view_source_center_x";
    public static final String EXTRA_REVEAL_VIEW_SOURCE_CENTER_Y = "reveal_view_source_center_y";
    public static final String EXTRA_USER = "user";
}
