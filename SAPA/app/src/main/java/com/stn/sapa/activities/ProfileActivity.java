package com.stn.sapa.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.stn.sapa.R;
import com.stn.sapa.managers.AppInfoManager;
import com.stn.sapa.managers.media.MediaManager;
import com.stn.sapa.models.UserProfile;
import com.stn.sapa.retrofit.general.RetrofitUtils;
import com.stn.sapa.retrofit.general.SapaRetrofitCallback;
import com.stn.sapa.utils.DialogUtils;
import com.stn.sapa.utils.GlideUtils;
import com.stn.sapa.z_base.BaseActivity;

import java.io.File;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class ProfileActivity extends BaseActivity {

    private RequestManager glideRequestManager;

    private TextInputLayout til_profile_activity_first_name;
    private TextInputLayout til_profile_activity_last_name;
    private TextInputLayout til_profile_activity_phone;

    private ImageView iv_profile_activity_photo;

    private InputMethodManager inputMethodManager;

    private File selectedProfilePhoto;

    @Override
    protected int getActivityLayoutId() {
        return R.layout.profile_activity;
    }

    @Override
    protected void linkUI() {
        til_profile_activity_first_name = (TextInputLayout) views.get(R.id.til_profile_activity_first_name);
        til_profile_activity_last_name = (TextInputLayout) views.get(R.id.til_profile_activity_last_name);
        til_profile_activity_phone = (TextInputLayout) views.get(R.id.til_profile_activity_phone);

        iv_profile_activity_photo = views.getImageView(R.id.iv_profile_activity_photo);
    }

    @Override
    protected void init() {
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        glideRequestManager = Glide.with(this);

        if (AppInfoManager.getInstance().getUserProfile() == null)
            RetrofitUtils.getInstance().getProfile(ProfileActivity.this, new SapaRetrofitCallback<UserProfile>() {
                @Override
                public void onSuccess(UserProfile response) {
                    AppInfoManager.getInstance().setUserProfile(response);
                    updateUiWithInfo();
                }

                @Override
                public void onFailure() {

                }
            });
        else
            updateUiWithInfo();

        if (til_profile_activity_phone.getEditText() != null)
            til_profile_activity_phone.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        inputMethodManager.hideSoftInputFromWindow(til_profile_activity_phone.getWindowToken(), 0);
                        til_profile_activity_phone.clearFocus();
                    }
                    return false;
                }
            });


    }

    @Override
    protected void setData() {
    }

    @Override
    protected void setActions() {
        views.get(R.id.iv_profile_activity_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaManager.getInstance(ProfileActivity.this).getPhoto(new MediaManager.OnMediaItemReceivedCallback<File>() {
                    @Override
                    public void onMediaItemReceived(File mediaItem) {
                        views.get(R.id.iv_profile_activity_photo).setPadding(0, 0, 0, 0);

                        selectedProfilePhoto = mediaItem;

                        glideRequestManager.load(selectedProfilePhoto).bitmapTransform(new CropCircleTransformation(ProfileActivity.this)).into(iv_profile_activity_photo);
                    }
                });
            }
        });

        views.get(R.id.tv_create_profile_activity_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (til_profile_activity_first_name.getEditText() != null && til_profile_activity_last_name.getEditText() != null && til_profile_activity_phone.getEditText() != null) {
                    DialogUtils.getInstance(ProfileActivity.this).showProgressDialog(R.string.please_wait);
                    RetrofitUtils.getInstance().saveProfile(ProfileActivity.this
                            , selectedProfilePhoto
                            , til_profile_activity_first_name.getEditText().getText().toString()
                            , til_profile_activity_last_name.getEditText().getText().toString()
                            , til_profile_activity_phone.getEditText().getText().toString()
                            , new SapaRetrofitCallback<Void>() {
                                @Override
                                public void onSuccess(Void response) {
                                    RetrofitUtils.getInstance().getProfile(ProfileActivity.this, new SapaRetrofitCallback<UserProfile>() {
                                        @Override
                                        public void onSuccess(UserProfile response) {
                                            AppInfoManager.getInstance().setUserProfile(response);

                                            DialogUtils.getInstance(ProfileActivity.this).dismissProgressDialog();

                                            HomeActivity.startActivity(ProfileActivity.this);
                                        }

                                        @Override
                                        public void onFailure() {

                                        }
                                    });
                                }

                                @Override
                                public void onFailure() {
                                    //TODO:Manage Errors
                                }
                            });
                }
            }
        });
    }

    private void updateUiWithInfo() {
        UserProfile userProfile = AppInfoManager.getInstance().getUserProfile();

        if (userProfile.getPhoto() != null) {
            views.get(R.id.iv_profile_activity_photo).setPadding(0, 0, 0, 0);
            glideRequestManager.load(GlideUtils.getGlideUrl(userProfile.getPhoto())).bitmapTransform(new CropCircleTransformation(this)).into(iv_profile_activity_photo);
        }

        if (til_profile_activity_first_name.getEditText() != null)
            til_profile_activity_first_name.getEditText().setText(userProfile.getFirst_name());

        if (til_profile_activity_last_name.getEditText() != null)
            til_profile_activity_last_name.getEditText().setText(userProfile.getLast_name());

        if (til_profile_activity_phone.getEditText() != null)
            til_profile_activity_phone.getEditText().setText(userProfile.getPhone());
    }

    public static void startActivity(Activity activity) {
        activity.startActivity(new Intent(activity, ProfileActivity.class));
    }

    public static void startActivityWithProfilePhotoTransition(Activity activity, ImageView iv_profile_photo) {
        Intent intent = new Intent(activity, ProfileActivity.class);
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(activity, iv_profile_photo, "profilePhoto");
        activity.startActivity(intent, options.toBundle());
    }
}
