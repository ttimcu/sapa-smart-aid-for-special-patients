package com.stn.sapa.retrofit.requestbodies;

/**
 * Created by Sebastian on 1/29/2017.
 */

public class UserRegisterBody {

    private String code;
    private String email;
    private String password;

    public UserRegisterBody(String code, String email, String password) {
        this.code = code;
        this.email = email;
        this.password = password;
    }
}
