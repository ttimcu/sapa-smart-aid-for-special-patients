package com.stn.sapa.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.stn.sapa.managers.SharedPreferencesManager;
import com.stn.sapa.retrofit.general.RetrofitUtils;
import com.stn.sapa.retrofit.general.SapaRetrofitCallback;

import java.io.IOException;


/**
 * Created by Sebastian on 8/17/2016.
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken("442994227927", GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.i(TAG, "GCM Registration Token: " + token);

            SharedPreferencesManager.getInstance(getApplicationContext()).setGcmToken(token);

            RetrofitUtils.getInstance().sendGCMToken(token, new SapaRetrofitCallback<Void>() {
                @Override
                public void onSuccess(Void response) {
                    
                }

                @Override
                public void onFailure() {

                }
            });

            subscribeTopics(token);
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
        }
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

}