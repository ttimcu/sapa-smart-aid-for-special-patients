package com.stn.sapa.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.view.View;

import com.stn.sapa.fragments.ForgotPasswordFragment;
import com.stn.sapa.fragments.LoginFragment;
import com.stn.sapa.fragments.RegisterFragment;

/**
 * Created by Sebastian on 1/30/2017.
 */

public class LoginAndRegisterViewPagerAdapter extends android.support.v13.app.FragmentStatePagerAdapter {

    private View.OnClickListener loginFragmentNewAccountClickListener;
    private View.OnClickListener loginFragmentForgotPasswordClickListener;

    public LoginAndRegisterViewPagerAdapter(FragmentManager fm, View.OnClickListener loginFragmentNewAccountClickListener, View.OnClickListener loginFragmentForgotPasswordClickListener) {
        super(fm);
        this.loginFragmentNewAccountClickListener = loginFragmentNewAccountClickListener;
        this.loginFragmentForgotPasswordClickListener = loginFragmentForgotPasswordClickListener;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new ForgotPasswordFragment();
                break;
            case 1:
                fragment = LoginFragment.getInstance(loginFragmentNewAccountClickListener, loginFragmentForgotPasswordClickListener);
                break;
            case 2:
                fragment = new RegisterFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
