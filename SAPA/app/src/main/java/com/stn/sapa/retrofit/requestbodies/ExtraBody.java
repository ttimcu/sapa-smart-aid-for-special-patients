package com.stn.sapa.retrofit.requestbodies;

/**
 * Created by Sebastian on 2/1/2017.
 */

public class ExtraBody {

    private String pulse;
    private String temperature;
    private String latitude;
    private String longitude;

    public ExtraBody(String pulse, String temperature, String latitude, String longitude) {
        this.pulse = pulse;
        this.temperature = temperature;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
