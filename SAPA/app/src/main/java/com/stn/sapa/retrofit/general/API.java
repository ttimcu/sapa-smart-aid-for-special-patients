package com.stn.sapa.retrofit.general;

import com.stn.sapa.models.UserProfile;
import com.stn.sapa.retrofit.requestbodies.DoctorGCMTokenBody;
import com.stn.sapa.retrofit.requestbodies.EmailRegistrationBody;
import com.stn.sapa.retrofit.requestbodies.ExtraBody;
import com.stn.sapa.retrofit.requestbodies.UserLoginBody;
import com.stn.sapa.retrofit.requestbodies.UserRegisterBody;
import com.stn.sapa.retrofit.requestbodies.UserRegistrationVerifyCodeBody;
import com.stn.sapa.retrofit.responsebodies.AddPatientResponse;
import com.stn.sapa.retrofit.responsebodies.DoctorVerifyCodeResponse;
import com.stn.sapa.retrofit.responsebodies.DoctorsAndPatientsResponse;
import com.stn.sapa.retrofit.responsebodies.LoginAndRegisterResponse;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by Sebastian on 1/29/2017.
 */

public interface API {

    String API_URL = "http://192.168.1.230:3000/";

    @POST("/doctor/email_registration")
    Call<Void> registerDoctorEmail(@Body EmailRegistrationBody emailRegistrationBody);

    @POST("/register/verify_code")
    Call<DoctorVerifyCodeResponse> verifyRegistrationCode(@Body UserRegistrationVerifyCodeBody userRegistrationVerifyCodeBody);

    @POST("/register")
    Call<LoginAndRegisterResponse> registerUser(@Body UserRegisterBody userRegisterBody);

    @POST("/login")
    Call<LoginAndRegisterResponse> loginUser(@Body UserLoginBody userLoginBody);

    @Multipart
    @POST("/profile")
    Call<Void> saveProfile(@PartMap HashMap<String, RequestBody> partMap, @Part MultipartBody.Part profilePhoto);

    @GET("/profile")
    Call<UserProfile> getProfile();

    @POST("/doctor/patient/add")
    Call<AddPatientResponse> addNewPatient(@Body EmailRegistrationBody emailRegistrationBody);

    @GET("/doctor/patients")
    Call<DoctorsAndPatientsResponse> getPatients(@Query("page") int page, @Query("count") int count);

    @POST("/doctor/gcm")
    Call<Void> sendGcmToken(@Body DoctorGCMTokenBody doctorGCMTokenBody);

    @POST("/extra")
    Call<Void> sendTempAndPulse(@Body ExtraBody extraBody);

    @GET("/patient/doctors")
    Call<DoctorsAndPatientsResponse> getDoctors(@Query("page") int page, @Query("count") int count);

}
