package com.stn.sapa.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.util.Patterns;
import android.view.View;

import com.stn.sapa.R;
import com.stn.sapa.retrofit.general.RetrofitUtils;
import com.stn.sapa.retrofit.general.SapaRetrofitCallback;
import com.stn.sapa.retrofit.responsebodies.AddPatientResponse;
import com.stn.sapa.utils.DialogUtils;
import com.stn.sapa.z_base.BaseActivityCircularReveal;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class AddNewPatientActivity extends BaseActivityCircularReveal {

    private TextInputLayout til_add_new_patient_activity;

    private String invalidEmailErrorString;

    @Override
    protected int getActivityLayoutId() {
        return R.layout.add_new_patient_activity;
    }

    @Override
    protected void linkUI() {
        til_add_new_patient_activity = (TextInputLayout) views.get(R.id.til_add_new_patient_activity);
    }

    @Override
    protected void init() {
        invalidEmailErrorString = getString(R.string.invalid_email);
    }

    @Override
    protected void setData() {

    }

    @Override
    protected void setActions() {
        views.get(R.id.b_add_new_patient_activity_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (til_add_new_patient_activity.getEditText() != null) {
                    String email = til_add_new_patient_activity.getEditText().getText().toString();

                    if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        DialogUtils.getInstance(AddNewPatientActivity.this).showProgressDialog(R.string.please_wait);
                        RetrofitUtils.getInstance().addNewPatient(AddNewPatientActivity.this, email, new SapaRetrofitCallback<AddPatientResponse>() {
                            @Override
                            public void onSuccess(AddPatientResponse response) {
                                DialogUtils.getInstance(AddNewPatientActivity.this).dismissProgressDialog();

                                if (response.getPatient().isRegistered())
                                    UserDetailsActivity.startActivity(AddNewPatientActivity.this, response.getPatient());

                                finish();
                            }

                            @Override
                            public void onFailure() {
                                //TODO:Manager Errors
                            }
                        });
                    } else {
                        til_add_new_patient_activity.setErrorEnabled(true);
                        til_add_new_patient_activity.setError(invalidEmailErrorString);
                    }
                }
            }
        });
    }

    public static void startActivity(Activity activity, View circularRevealSource) {
        Intent intent = new Intent(activity, AddNewPatientActivity.class);

        updateIntentWithSourceViewInfo(intent, circularRevealSource);

        activity.startActivity(intent);
    }
}
