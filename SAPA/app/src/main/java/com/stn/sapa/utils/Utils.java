package com.stn.sapa.utils;

import android.animation.Animator;
import android.view.View;
import android.view.ViewAnimationUtils;

import com.stn.sapa.constants.AnimationDurationConstants;


/**
 * Created by Sebastian on 9/21/2016.
 */
public class Utils {

    public static void circularRevealView(View revealView, int centerX, int centerY) {
        Animator circularReveal = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = Math.max(revealView.getWidth(), revealView.getHeight());

            circularReveal = ViewAnimationUtils.createCircularReveal(revealView, centerX, centerY, 0, finalRadius).setDuration(AnimationDurationConstants.DURATION_REVEAL_ACTIVITY);
        }

        revealView.setVisibility(View.VISIBLE);

        if (circularReveal != null)
            circularReveal.start();
    }

    public static void circularHideView(final View hideView, int centerX, int centerY, final Animator.AnimatorListener animatorListener) {
        Animator circularReveal = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = Math.max(hideView.getWidth(), hideView.getHeight());

            circularReveal = ViewAnimationUtils.createCircularReveal(hideView, centerX, centerY, finalRadius, 0).setDuration(AnimationDurationConstants.DURATION_REVEAL_ACTIVITY);
            circularReveal.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    animatorListener.onAnimationStart(animator);
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    hideView.setVisibility(View.INVISIBLE);
                    animatorListener.onAnimationEnd(animator);
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                    animatorListener.onAnimationCancel(animator);
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                    animatorListener.onAnimationRepeat(animator);
                }
            });
        } else {
            hideView.setVisibility(View.INVISIBLE);
            animatorListener.onAnimationEnd(null);
        }

        if (circularReveal != null)
            circularReveal.start();
    }

    public static int getViewXRelativeToRoot(View view) {
        if (view.getParent() == view.getRootView()) {
            return view.getLeft();
        } else {
            return view.getLeft() + getViewXRelativeToRoot((View) view.getParent());
        }
    }

    public static int getViewYRelativeToRoot(View view) {
        if (view.getParent() == view.getRootView()) {
            return view.getTop();
        } else {
            return view.getTop() + getViewYRelativeToRoot((View) view.getParent());
        }
    }
}
