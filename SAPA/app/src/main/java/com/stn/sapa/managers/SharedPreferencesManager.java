package com.stn.sapa.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sebastian on 1/30/2017.
 */

public class SharedPreferencesManager {

    private static final String SHARED_PREFERENCES_NAME = "SAPASharedPrefs";
    private static final String LOGIN_EMAIL = "login_email";
    private static final String LOGIN_PASSWORD = "login_password";
    private static final String GCM_TOKEN = "gcm_token";

    private static SharedPreferencesManager INSTANCE;

    private SharedPreferences sharedPreferences;

    public static SharedPreferencesManager getInstance(Context context) {
        if (INSTANCE == null)
            INSTANCE = new SharedPreferencesManager(context);

        return INSTANCE;
    }

    private SharedPreferencesManager(Context context) {
        this.sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void setLoginCredentials(String email, String password) {
        sharedPreferences.edit().putString(LOGIN_EMAIL, email).putString(LOGIN_PASSWORD, password).apply();
    }

    public String getLoginEmail() {
        return sharedPreferences.getString(LOGIN_EMAIL, null);
    }

    public String getLoginPassword() {
        return sharedPreferences.getString(LOGIN_PASSWORD, null);
    }

    public void setGcmToken(String gcmToken) {
        sharedPreferences.edit().putString(GCM_TOKEN, gcmToken).apply();
    }

    public String getGcmToken() {
        return sharedPreferences.getString(GCM_TOKEN, null);
    }

}
