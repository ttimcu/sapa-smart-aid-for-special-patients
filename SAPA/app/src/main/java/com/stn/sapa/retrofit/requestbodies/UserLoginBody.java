package com.stn.sapa.retrofit.requestbodies;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class UserLoginBody {

    private String email;
    private String password;

    public UserLoginBody(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
