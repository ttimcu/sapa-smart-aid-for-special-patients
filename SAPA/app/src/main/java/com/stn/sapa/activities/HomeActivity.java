package com.stn.sapa.activities;

import android.animation.Animator;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;
import com.stn.sapa.R;
import com.stn.sapa.adapters.DoctorsAndPatientsAdapter;
import com.stn.sapa.gcm.RegistrationIntentService;
import com.stn.sapa.managers.AppInfoManager;
import com.stn.sapa.managers.LocationManager;
import com.stn.sapa.managers.SharedPreferencesManager;
import com.stn.sapa.models.User;
import com.stn.sapa.retrofit.general.RetrofitUtils;
import com.stn.sapa.retrofit.general.SapaRetrofitCallback;
import com.stn.sapa.retrofit.responsebodies.DoctorsAndPatientsResponse;
import com.stn.sapa.z_base.BaseActivity;

/**
 * Created by Sebastian on 1/30/2017.
 */

public class HomeActivity extends BaseActivity {

    private static final String TAG = "HomeActivity";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private DrawerLayout dl_home_activity;

    private Toolbar toolbar_home_activity;
    private AppBarLayout abl_main_activity;
    private RecyclerView rv_doctor_home_fragment;

    private FrameLayout fl_home_activity_patient_info;
    private ImageView iv_home_activity_patient_pulse;
    private TextView tv_home_activity_patient_pulse;
    private ImageView iv_home_activity_patient_temp;
    private TextView tv_home_activity_patient_temp;
    private TextView tv_home_activity_patient_location_lat;
    private TextView tv_home_activity_patient_location_lng;

    private FrameLayout fl_home_activity_doctor_map_overlay;

    private float tempValue = 37.0f;
    private boolean tempShouldGoUp = true;

    private int pulseValue = 86;
    private boolean pulseShouldGoUp = true;
    private boolean heartShouldGrow = true;

    private Handler handler;
    private Runnable pulseRunnable;
    private Runnable tempRunnable;
    private Runnable sendTempAndPulseRunnable;

    private GoogleMap googleMap;

    @Override
    protected int getActivityLayoutId() {
        return R.layout.home_activity;
    }

    @Override
    protected void linkUI() {
        dl_home_activity = (DrawerLayout) views.get(R.id.dl_home_activity);
        toolbar_home_activity = (Toolbar) views.get(R.id.toolbar_home_activity);
        abl_main_activity = (AppBarLayout) views.get(R.id.abl_main_activity);
        fl_home_activity_patient_info = views.getFrameLayout(R.id.fl_home_activity_patient_info);
        iv_home_activity_patient_pulse = views.getImageView(R.id.iv_home_activity_patient_pulse);
        tv_home_activity_patient_pulse = views.getTextView(R.id.tv_home_activity_patient_pulse);
        iv_home_activity_patient_temp = views.getImageView(R.id.iv_home_activity_patient_temp);
        tv_home_activity_patient_temp = views.getTextView(R.id.tv_home_activity_patient_temp);
        tv_home_activity_patient_location_lat = views.getTextView(R.id.tv_home_activity_patient_location_lat);
        tv_home_activity_patient_location_lng = views.getTextView(R.id.tv_home_activity_patient_location_lng);

        fl_home_activity_doctor_map_overlay = views.getFrameLayout(R.id.fl_home_activity_doctor_map_overlay);

        rv_doctor_home_fragment = views.getRecyclerView(R.id.rv_home_activity);
    }

    @Override
    protected void init() {
        setSupportActionBar(toolbar_home_activity);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(getString(R.string.smart_aid_for_patient_assistance));
        collapsingToolbarLayout.setExpandedTitleColor(Color.argb(0, 0, 0, 0));
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.rgb(255, 255, 255));

        if (SharedPreferencesManager.getInstance(this).getGcmToken() == null && AppInfoManager.getInstance().getUserProfile().is_doctor() && checkPlayServices())
            startService(new Intent(this, RegistrationIntentService.class));

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, dl_home_activity, toolbar_home_activity, R.string.drawer_open, R.string.drawer_close);
        dl_home_activity.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        rv_doctor_home_fragment.setLayoutManager(new LinearLayoutManager(this));

        if (!AppInfoManager.getInstance().getUserProfile().is_doctor()) {
            abl_main_activity.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            views.get(R.id.fab_home_activity).setVisibility(View.GONE);
            fl_home_activity_patient_info.setVisibility(View.VISIBLE);

            handler = new Handler();

            pulseRunnable = new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_home_activity_patient_pulse.setText(String.valueOf(pulseValue));
                        }
                    });

                    if (pulseValue == 150)
                        pulseShouldGoUp = false;
                    else if (pulseValue == 40)
                        pulseShouldGoUp = true;

                    if (pulseShouldGoUp)
                        pulseValue++;
                    else
                        pulseValue--;

                    handler.postDelayed(this, 1000);
                }
            };

            tempRunnable = new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_home_activity_patient_temp.setText(String.valueOf(tempValue));
                        }
                    });

                    if (tempValue > 41)
                        tempShouldGoUp = false;
                    else if (tempValue < 30)
                        tempShouldGoUp = true;

                    if (tempShouldGoUp)
                        tempValue++;
                    else
                        tempValue--;

                    handler.postDelayed(this, 3000);
                }
            };

            sendTempAndPulseRunnable = new Runnable() {
                @Override
                public void run() {
                    LocationManager.getInstance(HomeActivity.this).getLastKnownLocation(new LocationManager.OnLocationReceivedCallback() {
                        @Override
                        public void onLocationReceived(final Location location) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    tv_home_activity_patient_location_lat.setText(String.valueOf(location.getLatitude()));
                                    tv_home_activity_patient_location_lng.setText(String.valueOf(location.getLongitude()));

                                    RetrofitUtils.getInstance().sendTempPulseAndLocation(HomeActivity.this, pulseValue, tempValue, location.getLatitude(), location.getLongitude(), new SapaRetrofitCallback<Void>() {
                                        @Override
                                        public void onSuccess(Void response) {

                                        }

                                        @Override
                                        public void onFailure() {

                                        }
                                    });
                                }
                            });
                        }
                    });

                    handler.postDelayed(this, 10000);
                }
            };


            iv_home_activity_patient_pulse.animate().scaleX(0.8f).scaleY(0.8f).setDuration(1000).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if (heartShouldGrow) {
                        iv_home_activity_patient_pulse.animate().scaleX(1.0f).scaleY(1.0f).setDuration(1000).setListener(this);
                        heartShouldGrow = false;
                    } else {
                        iv_home_activity_patient_pulse.animate().scaleX(0.8f).scaleY(0.8f).setDuration(1000).setListener(this);
                        heartShouldGrow = true;
                    }
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            handler.post(pulseRunnable);
            handler.post(tempRunnable);
            handler.postDelayed(sendTempAndPulseRunnable, 2000);
        } else {
            fl_home_activity_doctor_map_overlay.setVisibility(View.VISIBLE);
            MapFragment mMapFragment = MapFragment.newInstance();
            FragmentTransaction fragmentTransaction =
                    getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.map_home_activity, mMapFragment);
            fragmentTransaction.commit();

            mMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    HomeActivity.this.googleMap = googleMap;
                }
            });
        }
    }

    @Override
    protected void setData() {
    }

    @Override
    protected void setActions() {
        views.get(R.id.fab_home_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNewPatientActivity.startActivity(HomeActivity.this, views.get(R.id.fab_home_activity));
            }
        });
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppInfoManager.getInstance().getUserProfile().is_doctor()) {
            RetrofitUtils.getInstance().getDoctors(this, 0, new SapaRetrofitCallback<DoctorsAndPatientsResponse>() {
                @Override
                public void onSuccess(DoctorsAndPatientsResponse response) {
                    rv_doctor_home_fragment.setAdapter(new DoctorsAndPatientsAdapter(HomeActivity.this, response.getUsers()));
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            RetrofitUtils.getInstance().getPatients(this, 0, new SapaRetrofitCallback<DoctorsAndPatientsResponse>() {
                @Override
                public void onSuccess(DoctorsAndPatientsResponse response) {
                    boolean zoomWasMade = false;
                    for (User user : response.getUsers()) {
                        if (user.isRegistered() && user.getExtra() != null && user.getExtra().getLatitude() != null) {
                            MyItem myItem = new MyItem(Double.valueOf(user.getExtra().getLatitude()), Double.valueOf(user.getExtra().getLongitude()));
                            googleMap.addMarker(new MarkerOptions().position(myItem.getPosition()));

                            if (!zoomWasMade) {
                                zoomWasMade = true;
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myItem.getPosition(), 9.0f));
                            }
                        }
                    }
                    rv_doctor_home_fragment.setAdapter(new DoctorsAndPatientsAdapter(HomeActivity.this, response.getUsers()));
                }

                @Override
                public void onFailure() {

                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isFinishing() && !AppInfoManager.getInstance().getUserProfile().is_doctor()) {
            handler.removeCallbacks(pulseRunnable);
            handler.removeCallbacks(tempRunnable);
            handler.removeCallbacks(sendTempAndPulseRunnable);
        }
    }

    public static void startActivity(Activity activity) {
        activity.startActivity(new Intent(activity, HomeActivity.class));
        activity.finish();
    }

    public class MyItem implements ClusterItem {
        private final LatLng mPosition;

        public MyItem(double lat, double lng) {
            mPosition = new LatLng(lat, lng);
        }

        @Override
        public LatLng getPosition() {
            return mPosition;
        }
    }
}
