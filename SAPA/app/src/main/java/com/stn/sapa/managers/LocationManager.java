package com.stn.sapa.managers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.stn.sapa.R;
import com.stn.sapa.constants.PermissionRequestsContants;


/**
 * Created by Sebastian on 7/7/2016.
 */
public class LocationManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "LocationManager";

    private static LocationManager instance;
    private static Activity currentActivity;

    private GoogleApiClient googleApiClient;
    private OnLocationReceivedCallback currentCallback;

    public static LocationManager getInstance(Activity activity) {
        if (instance == null || !currentActivity.getLocalClassName().contentEquals(activity.getLocalClassName()) || currentActivity.isFinishing()) {
            currentActivity = activity;
            instance = new LocationManager();
        }

        return instance;
    }

    private LocationManager() {
        googleApiClient = new GoogleApiClient.Builder(currentActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLastKnownLocation(currentCallback);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection " + i + " was suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.getErrorMessage() != null)
            Log.d(TAG, connectionResult.getErrorMessage());
    }

    public void getLastKnownLocation(OnLocationReceivedCallback callback) {
        this.currentCallback = callback;

        if (currentCallback != null) {
            if (ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PermissionRequestsContants.PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE);
            } else {

                if (googleApiClient.isConnected()) {
                    android.location.LocationManager locationManager = (android.location.LocationManager) currentActivity.getSystemService(Context.LOCATION_SERVICE);

                    if (!locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
                        new AlertDialog.Builder(currentActivity).setTitle(R.string.use_location).setMessage(R.string.the_location_services_must_be_enabled_in_order_to).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                currentActivity.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                currentCallback = null;
                            }
                        }).show();
                    } else {
                        Location lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

                        Log.d(TAG, currentCallback == null ? "NULL" : "IS NOT NULL");

                        if (lastKnownLocation != null && currentCallback != null) {
                            currentCallback.onLocationReceived(lastKnownLocation);
                            currentCallback = null;
                        } else if (lastKnownLocation == null) {
                            getLastKnownLocation(currentCallback);
                        }
                    }

                }
            }
        }
    }

    public void connect() {
        googleApiClient.connect();
    }

    public void disconnect() {
        googleApiClient.disconnect();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionRequestsContants.PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    getLastKnownLocation(currentCallback);
                else
                    currentCallback = null;
                break;
        }
    }

    public interface OnLocationReceivedCallback {
        void onLocationReceived(Location location);
    }
}
