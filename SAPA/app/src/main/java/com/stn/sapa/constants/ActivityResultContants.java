package com.stn.sapa.constants;

/**
 * Created by Sebastian on 7/26/2016.
 */
public class ActivityResultContants {
    public static final int GALLERY_PHOTO_REQUEST_CODE = 100;
    public static final int CAPTURE_IMAGE_REQUEST_CODE = 200;
}
