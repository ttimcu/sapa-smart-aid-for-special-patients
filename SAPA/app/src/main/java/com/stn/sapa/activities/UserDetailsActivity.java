package com.stn.sapa.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.stn.sapa.R;
import com.stn.sapa.adapters.UserDetailsListAdapter;
import com.stn.sapa.constants.IntentExtraConstants;
import com.stn.sapa.managers.AppInfoManager;
import com.stn.sapa.models.PrescriptionAndDisease;
import com.stn.sapa.models.User;
import com.stn.sapa.z_base.BaseActivity;

import java.util.ArrayList;

/**
 * Created by Sebastian on 2/1/2017.
 */

public class UserDetailsActivity extends BaseActivity {

    private TextView b_left_menu_fragment_add_prescription;
    private TextView b_left_menu_fragment_add_disease;
    private RecyclerView rv_user_details_activity;

    private User user;

    @Override
    protected int getActivityLayoutId() {
        return R.layout.user_details_activity;
    }

    @Override
    protected void linkUI() {
        b_left_menu_fragment_add_prescription = views.getTextView(R.id.b_left_menu_fragment_add_prescription);
        b_left_menu_fragment_add_disease = views.getTextView(R.id.b_left_menu_fragment_add_disease);
        rv_user_details_activity = views.getRecyclerView(R.id.rv_user_details_activity);
    }

    @Override
    protected void init() {
        user = (User) getIntent().getExtras().getSerializable(IntentExtraConstants.EXTRA_USER);
        if (AppInfoManager.getInstance().getUserProfile().is_doctor()) {
            views.get(R.id.ll_user_details_activity_doctor_buttons).setVisibility(View.VISIBLE);
        }

        if (user != null) {


            rv_user_details_activity.setLayoutManager(new LinearLayoutManager(this));

            ArrayList<PrescriptionAndDisease> items = new ArrayList<>();
            if (user.getExtra().getDiseases() != null)
                for (PrescriptionAndDisease disease : user.getExtra().getDiseases()) {
                    disease.setResourceId(R.drawable.ic_bug);
                    items.add(disease);
                }

            if (user.getExtra().getPrescriptions() != null)
                for (PrescriptionAndDisease prescription : user.getExtra().getPrescriptions()) {
                    prescription.setResourceId(R.drawable.ic_border_color_black_24dp);
                    items.add(prescription);
                }

            rv_user_details_activity.setAdapter(new UserDetailsListAdapter(this, user, items));
        }
    }

    @Override
    protected void setData() {

    }

    @Override
    protected void setActions() {

    }

    public static void startActivityWithSharedElements(Activity activity, User user, ImageView profilePhoto, TextView userName, TextView userEmail, TextView userPhone) {
        Intent intent = new Intent(activity, UserDetailsActivity.class);

        intent.putExtra(IntentExtraConstants.EXTRA_USER, user);
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(activity
                        , new Pair<View, String>(profilePhoto, "profilePhoto")
                        , new Pair<View, String>(userName, "userName")
                        , new Pair<View, String>(userEmail, "userEmail")
                        , new Pair<View, String>(userPhone, "userPhone"));

        activity.startActivity(intent, options.toBundle());
    }

    public static void startActivity(Activity activity, User user) {
        Intent intent = new Intent(activity, UserDetailsActivity.class);

        intent.putExtra(IntentExtraConstants.EXTRA_USER, user);

        activity.startActivity(intent);
    }
}
