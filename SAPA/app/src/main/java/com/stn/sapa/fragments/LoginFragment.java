package com.stn.sapa.fragments;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.stn.sapa.R;
import com.stn.sapa.activities.HomeActivity;
import com.stn.sapa.managers.AppInfoManager;
import com.stn.sapa.managers.SharedPreferencesManager;
import com.stn.sapa.models.UserProfile;
import com.stn.sapa.retrofit.general.RetrofitUtils;
import com.stn.sapa.retrofit.general.SapaRetrofitCallback;
import com.stn.sapa.retrofit.responsebodies.LoginAndRegisterResponse;
import com.stn.sapa.utils.DialogUtils;
import com.stn.sapa.z_base.BaseFragment;

/**
 * Created by Sebastian on 1/29/2017.
 */

public class LoginFragment extends BaseFragment {

    private View.OnClickListener loginFragmentNewAccountClickListener;
    private View.OnClickListener loginFragmentForgotPasswordClickListener;

    private TextInputLayout til_login_fragment_email;
    private TextInputLayout til_login_fragment_password;

    private String invalidEmailErrorString;
    private String invalidPasswordErrorString;

    private InputMethodManager inputMethodManager;

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.login_fragment;
    }

    @Override
    protected void linkUI() {
        til_login_fragment_email = (TextInputLayout) views.get(R.id.til_login_fragment_email);
        til_login_fragment_password = (TextInputLayout) views.get(R.id.til_login_fragment_password);
    }

    @Override
    protected void init() {
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        invalidEmailErrorString = getString(R.string.invalid_email);
        invalidPasswordErrorString = getString(R.string.invalid_password);

        if (til_login_fragment_password.getEditText() != null)
            til_login_fragment_password.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        inputMethodManager.hideSoftInputFromWindow(til_login_fragment_password.getWindowToken(), 0);
                        til_login_fragment_password.clearFocus();
                    }
                    return false;
                }
            });
    }

    @Override
    protected void setData() {
        String savedEmail = SharedPreferencesManager.getInstance(getActivity()).getLoginEmail();
        String savedPassword = SharedPreferencesManager.getInstance(getActivity()).getLoginPassword();

        if (til_login_fragment_email.getEditText() != null && savedEmail != null)
            til_login_fragment_email.getEditText().setText(savedEmail);

        if (til_login_fragment_password.getEditText() != null && savedPassword != null)
            til_login_fragment_password.getEditText().setText(savedPassword);
    }

    @Override
    protected void setActions() {
        views.get(R.id.tv_login_fragment_new_account).setOnClickListener(loginFragmentNewAccountClickListener);
        views.get(R.id.tv_login_fragment_forgot_password).setOnClickListener(loginFragmentForgotPasswordClickListener);

        views.get(R.id.fab_login_fragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (til_login_fragment_email.getEditText() != null && til_login_fragment_password.getEditText() != null) {
                    //TODO:Show ProgressDialog

                    til_login_fragment_email.setErrorEnabled(false);
                    til_login_fragment_password.setErrorEnabled(false);

                    final String email = til_login_fragment_email.getEditText().getText().toString();
                    final String password = til_login_fragment_password.getEditText().getText().toString();

                    if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        DialogUtils.getInstance(getActivity()).showProgressDialog(R.string.please_wait);
                        RetrofitUtils.getInstance().loginUser(getActivity(), email, password, new SapaRetrofitCallback<LoginAndRegisterResponse>() {
                            @Override
                            public void onSuccess(LoginAndRegisterResponse response) {
                                SharedPreferencesManager.getInstance(getActivity()).setLoginCredentials(email, password);

                                AppInfoManager.getInstance().setAuthorizationToken(response.getToken());

                                RetrofitUtils.getInstance().getProfile(getActivity(), new SapaRetrofitCallback<UserProfile>() {
                                    @Override
                                    public void onSuccess(UserProfile response) {
                                        AppInfoManager.getInstance().setUserProfile(response);

                                        DialogUtils.getInstance(getActivity()).dismissProgressDialog();

                                        HomeActivity.startActivity(getActivity());
                                    }

                                    @Override
                                    public void onFailure() {

                                    }
                                });
                            }

                            @Override
                            public void onFailure() {
                                til_login_fragment_email.setErrorEnabled(true);
                                til_login_fragment_email.setError(invalidEmailErrorString);
                                til_login_fragment_password.setErrorEnabled(true);
                                til_login_fragment_password.setError(invalidPasswordErrorString);
                            }
                        });
                    } else {
                        til_login_fragment_email.setErrorEnabled(true);
                        til_login_fragment_email.setError(invalidEmailErrorString);
                    }
                }
            }
        });
    }

    public void setLoginFragmentNewAccountClickListener(View.OnClickListener loginFragmentNewAccountClickListener) {
        this.loginFragmentNewAccountClickListener = loginFragmentNewAccountClickListener;
    }

    public void setLoginFragmentForgotPasswordClickListener(View.OnClickListener loginFragmentForgotPasswordClickListener) {
        this.loginFragmentForgotPasswordClickListener = loginFragmentForgotPasswordClickListener;
    }

    public static LoginFragment getInstance(View.OnClickListener loginFragmentNewAccountClickListener, View.OnClickListener loginFragmentForgotPasswordClickListener) {
        LoginFragment fragment = new LoginFragment();

        fragment.setLoginFragmentForgotPasswordClickListener(loginFragmentForgotPasswordClickListener);
        fragment.setLoginFragmentNewAccountClickListener(loginFragmentNewAccountClickListener);

        return fragment;
    }
}
