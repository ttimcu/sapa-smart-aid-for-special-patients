package com.stn.sapa.retrofit.responsebodies;

import com.stn.sapa.models.User;

/**
 * Created by Sebastian on 2/1/2017.
 */

public class AddPatientResponse {

    private User patient;

    public User getPatient() {
        return patient;
    }
}
