package com.stn.sapa.models;

import java.io.Serializable;

/**
 * Created by Sebastian on 2/1/2017.
 */

public class PrescriptionAndDisease implements Serializable {

    private int resourceId;
    private String title;
    private String content;

    public PrescriptionAndDisease(int resourceId, String title, String content) {
        this.resourceId = resourceId;
        this.title = title;
        this.content = content;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
