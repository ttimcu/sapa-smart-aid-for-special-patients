package com.stn.sapa.activities;

import android.view.View;

import com.stn.sapa.R;
import com.stn.sapa.adapters.LoginAndRegisterViewPagerAdapter;
import com.stn.sapa.customviews.NonSwipeableViewPager;
import com.stn.sapa.managers.SharedPreferencesManager;
import com.stn.sapa.z_base.BaseActivity;

/**
 * Created by Sebastian on 1/29/2017.
 */

public class MainActivity extends BaseActivity {

    public static NonSwipeableViewPager vp_main_activity;

    @Override
    protected int getActivityLayoutId() {
        return R.layout.main_activity;
    }

    @Override
    protected void linkUI() {
        vp_main_activity = (NonSwipeableViewPager) views.get(R.id.vp_main_activity);
    }

    @Override
    protected void init() {
        SharedPreferencesManager.getInstance(this).setGcmToken(null);
        vp_main_activity.setAdapter(new LoginAndRegisterViewPagerAdapter(getFragmentManager(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //NEW ACCOUNT CLICK
                vp_main_activity.setCurrentItem(2, true);
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //FORGOT PASSWORD CLICK
                vp_main_activity.setCurrentItem(0, true);
            }
        }));

        vp_main_activity.setCurrentItem(1);
    }

    @Override
    protected void setData() {

    }

    @Override
    protected void setActions() {

    }

    @Override
    public void onBackPressed() {
        int vpItemPos = vp_main_activity.getCurrentItem();

        if (vpItemPos == 0 || vpItemPos == 2)
            vp_main_activity.setCurrentItem(1, true);
        else
            super.onBackPressed();
    }
}
