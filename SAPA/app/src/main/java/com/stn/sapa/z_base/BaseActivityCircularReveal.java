package com.stn.sapa.z_base;

import android.animation.Animator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;

import com.stn.sapa.R;
import com.stn.sapa.constants.IntentExtraConstants;
import com.stn.sapa.utils.Utils;


/**
 * Created by Sebastian on 12/9/2016.
 */

public abstract class BaseActivityCircularReveal extends BaseActivity {

    private int revealCenterX;
    private int revealCenterY;

    @Override
    protected void doStuffBeforeSetContentView() {
        super.doStuffBeforeSetContentView();
        overridePendingTransition(R.anim.do_not_move, R.anim.do_not_move);
    }

    @Override
    protected void doStuffBeforeLinkUI() {
        super.doStuffBeforeLinkUI();
        final Bundle extras = getIntent().getExtras();

        revealCenterX = extras.getInt(IntentExtraConstants.EXTRA_REVEAL_VIEW_SOURCE_CENTER_X);
        revealCenterY = extras.getInt(IntentExtraConstants.EXTRA_REVEAL_VIEW_SOURCE_CENTER_Y);

        views.root.setVisibility(View.INVISIBLE);

        ViewTreeObserver viewTreeObserver = views.root.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Utils.circularRevealView(views.root, revealCenterX, revealCenterY);

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        views.root.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        views.root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        Utils.circularHideView(views.root, revealCenterX, revealCenterY, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                finish();
                overridePendingTransition(R.anim.do_not_move, R.anim.do_not_move);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }


    protected static void updateIntentWithSourceViewInfo(Intent intent, View source) {
        intent.putExtra(IntentExtraConstants.EXTRA_REVEAL_VIEW_SOURCE_CENTER_X, Utils.getViewXRelativeToRoot(source) + source.getWidth() / 2);
        intent.putExtra(IntentExtraConstants.EXTRA_REVEAL_VIEW_SOURCE_CENTER_Y, Utils.getViewYRelativeToRoot(source));
    }
}
