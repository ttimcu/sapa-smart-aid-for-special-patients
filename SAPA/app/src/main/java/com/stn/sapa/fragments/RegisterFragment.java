package com.stn.sapa.fragments;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.stn.sapa.R;
import com.stn.sapa.activities.ProfileActivity;
import com.stn.sapa.managers.AppInfoManager;
import com.stn.sapa.retrofit.general.RetrofitUtils;
import com.stn.sapa.retrofit.general.SapaRetrofitCallback;
import com.stn.sapa.retrofit.responsebodies.DoctorVerifyCodeResponse;
import com.stn.sapa.retrofit.responsebodies.LoginAndRegisterResponse;
import com.stn.sapa.utils.DialogUtils;
import com.stn.sapa.z_base.BaseFragment;

/**
 * Created by Sebastian on 1/30/2017.
 */

public class RegisterFragment extends BaseFragment {

    private static final int REGISTRATION_CODE_LENGTH = 6;

    private TextInputLayout til_register_fragment_registration_code;
    private ProgressBar pb_register_fragment_registration_code;
    private ImageView iv_register_fragment_registration_code_check;

    private TextInputLayout til_register_fragment_email;

    private TextInputLayout til_register_fragment_password;
    private TextInputLayout til_register_fragment_re_type_password;

    private FloatingActionButton fab_register_fragment_register;

    private String passwordsDoNotMatchError;
    private String invalidRegistrationCodeError;

    private InputMethodManager inputMethodManager;

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.register_fragment;
    }

    @Override
    protected void linkUI() {
        til_register_fragment_registration_code = (TextInputLayout) views.get(R.id.til_register_fragment_registration_code);
        pb_register_fragment_registration_code = (ProgressBar) views.get(R.id.pb_register_fragment_registration_code);
        iv_register_fragment_registration_code_check = views.getImageView(R.id.iv_register_fragment_registration_code_check);

        til_register_fragment_email = (TextInputLayout) views.get(R.id.til_register_fragment_email);

        til_register_fragment_password = (TextInputLayout) views.get(R.id.til_register_fragment_password);
        til_register_fragment_re_type_password = (TextInputLayout) views.get(R.id.til_register_fragment_re_type_password);

        fab_register_fragment_register = (FloatingActionButton) views.get(R.id.fab_register_fragment_register);
    }

    @Override
    protected void init() {
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        til_register_fragment_email.setEnabled(false);
        invalidRegistrationCodeError = getString(R.string.invalid_registration_code);
        passwordsDoNotMatchError = getString(R.string.passwords_do_not_match);

        if (til_register_fragment_re_type_password.getEditText() != null)
            til_register_fragment_re_type_password.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        inputMethodManager.hideSoftInputFromWindow(til_register_fragment_re_type_password.getWindowToken(), 0);
                        til_register_fragment_re_type_password.clearFocus();
                    }
                    return false;
                }
            });

        if (til_register_fragment_registration_code.getEditText() != null) {
            final EditText et_register_fragment_registration_code = til_register_fragment_registration_code.getEditText();

            et_register_fragment_registration_code.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    til_register_fragment_registration_code.setErrorEnabled(false);
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.length() == REGISTRATION_CODE_LENGTH) {

                        showRegistrationCodeProgressBar();

                        RetrofitUtils.getInstance().verifyRegistrationCode(getActivity(), editable.toString(), new SapaRetrofitCallback<DoctorVerifyCodeResponse>() {
                            @Override
                            public void onSuccess(DoctorVerifyCodeResponse response) {
                                hideRegistrationCodeProgressBar();
                                iv_register_fragment_registration_code_check.setVisibility(View.VISIBLE);

                                if (til_register_fragment_email.getEditText() != null)
                                    til_register_fragment_email.getEditText().setText(response.getEmail());

                                til_register_fragment_email.setVisibility(View.VISIBLE);
                                til_register_fragment_password.requestFocus();
                                til_register_fragment_registration_code.setEnabled(false);
                            }

                            @Override
                            public void onFailure() {
                                til_register_fragment_registration_code.setErrorEnabled(true);
                                til_register_fragment_registration_code.setError(invalidRegistrationCodeError);
                                til_register_fragment_registration_code.setEnabled(true);
                                et_register_fragment_registration_code.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_light));

                                hideRegistrationCodeProgressBar();
                            }
                        });
                    }
                }
            });
        }

        fab_register_fragment_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (til_register_fragment_registration_code.getEditText() != null && til_register_fragment_password.getEditText() != null && til_register_fragment_re_type_password.getEditText() != null && til_register_fragment_email.getEditText() != null) {
                    String password = til_register_fragment_password.getEditText().getText().toString();
                    String reTypedPassword = til_register_fragment_re_type_password.getEditText().getText().toString();

                    til_register_fragment_re_type_password.setErrorEnabled(false);

                    //TODO:Show ProgressDialog

                    if (password.contentEquals(reTypedPassword)) {
                        DialogUtils.getInstance(getActivity()).showProgressDialog(R.string.please_wait);

                        RetrofitUtils.getInstance().registerUser(getActivity()
                                , til_register_fragment_registration_code.getEditText().getText().toString()
                                , til_register_fragment_email.getEditText().getText().toString()
                                , password
                                , new SapaRetrofitCallback<LoginAndRegisterResponse>() {
                                    @Override
                                    public void onSuccess(LoginAndRegisterResponse response) {
                                        AppInfoManager.getInstance().setAuthorizationToken(response.getToken());

                                        DialogUtils.getInstance(getActivity()).dismissProgressDialog();

                                        ProfileActivity.startActivity(getActivity());
                                        getActivity().finish();
                                    }

                                    @Override
                                    public void onFailure() {

                                    }
                                });
                    } else {
                        til_register_fragment_re_type_password.setErrorEnabled(true);
                        til_register_fragment_re_type_password.setError(passwordsDoNotMatchError);
                    }
                }
            }
        });
    }

    @Override
    protected void setData() {

    }

    @Override
    protected void setActions() {

    }

    private void showRegistrationCodeProgressBar() {
        pb_register_fragment_registration_code.setVisibility(View.VISIBLE);
    }

    private void hideRegistrationCodeProgressBar() {
        pb_register_fragment_registration_code.setVisibility(View.GONE);
    }
}
