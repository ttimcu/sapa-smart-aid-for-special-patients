package com.stn.sapa.retrofit.responsebodies;

import com.stn.sapa.models.User;

import java.util.ArrayList;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class DoctorsAndPatientsResponse {

    private ArrayList<User> users;

    public ArrayList<User> getUsers() {
        return users;
    }
}
