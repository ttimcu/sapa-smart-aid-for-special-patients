package com.stn.sapa.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.stn.sapa.R;
import com.stn.sapa.activities.MainActivity;
import com.stn.sapa.retrofit.general.RetrofitUtils;
import com.stn.sapa.retrofit.general.SapaRetrofitCallback;
import com.stn.sapa.z_base.BaseFragment;

/**
 * Created by Sebastian on 1/30/2017.
 */

public class ForgotPasswordFragment extends BaseFragment {

    private EditText et_doctor_registration_email;
    private Button b_doctor_registration_email;

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.forgot_password_fragment;
    }

    @Override
    protected void linkUI() {
        et_doctor_registration_email = views.getEditText(R.id.et_doctor_registration_email);
        b_doctor_registration_email = views.getButton(R.id.b_doctor_registration_email);
    }

    @Override
    protected void init() {

    }

    @Override
    protected void setData() {

    }

    @Override
    protected void setActions() {
        b_doctor_registration_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RetrofitUtils.getInstance().registerDoctorEmail(getActivity(), et_doctor_registration_email.getText().toString(), new SapaRetrofitCallback<Void>() {
                    @Override
                    public void onSuccess(Void response) {
                        MainActivity.vp_main_activity.setCurrentItem(2, true);
                    }

                    @Override
                    public void onFailure() {

                    }
                });
            }
        });
    }
}
