package com.stn.sapa.retrofit.general;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sebastian on 1/29/2017.
 */

public abstract class SapaRetrofitCallback<T> implements Callback<T> {

    private static final String TAG = "ThrillshareRetrCallback";

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        RetrofitUtils.getInstance().removePendingCallFromMap(call);

        for (String headerName : response.headers().names())
            if (!onResponseHeader(headerName, response.headers().get(headerName)))
                break;

        if (response.isSuccessful() && response.code() >= 200 && response.code() < 300)
            onSuccess(response.body());
        else
            onFailure();

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        RetrofitUtils.getInstance().removePendingCallFromMap(call);
        Log.e(TAG, "ERROR!", t);
    }

    public boolean onResponseHeader(String name, String value) {
        return false;
    }

    public abstract void onSuccess(T response);

    public abstract void onFailure();
}
