package com.stn.sapa.z_base;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Sebi Ursulescu on 7/5/2015.
 */
public abstract class BaseFragment extends Fragment {

    protected ViewsHolder views;

    protected abstract int getFragmentLayoutId();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getFragmentLayoutId(), null);
        view.setClickable(true);

        views = new ViewsHolder(view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        linkUI();
        init();
        setData();
        setActions();
    }

    protected abstract void linkUI();

    protected abstract void init();

    protected abstract void setData();

    protected abstract void setActions();

    protected BaseFragmentsActivity getBaseActivity() {
        return (BaseFragmentsActivity) getActivity();
    }
}
