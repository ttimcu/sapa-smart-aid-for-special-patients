package com.stn.sapa.z_base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Sebastian on 9/3/2015.
 */
public abstract class BaseRecyclerViewHeaderAndFooterAdapter extends RecyclerView.Adapter<ViewsHolder> {

    private static final int HEADER = Integer.MIN_VALUE;
    private static final int FOOTER = Integer.MAX_VALUE;

    protected Context context;
    protected LayoutInflater inflater;

    /**
     * The constructor for the BaseRecyclerHeaderAndFooterAdater
     *
     * @param context The context you are in from where the inflater will be initialized
     */
    public BaseRecyclerViewHeaderAndFooterAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER) {
            View v = inflater.inflate(getHeaderLayoutId(), parent, false);
            return new ViewsHolder(v);
        } else if (viewType == FOOTER) {
            View v = inflater.inflate(getFooterLayoutId(), parent, false);
            return new ViewsHolder(v);
        } else {
            return onViewHolderCreate(parent, viewType);
        }
    }

    /**
     * The method where the {@link ViewsHolder ViewsHolder} for the item is created
     *
     * @param parent   The parent of the {@link ViewsHolder ViewsHolder}
     * @param viewType The view type returned by the {@link #getViewType(int) getViewType} method
     * @return The {@link ViewsHolder ViewsHolder} object of the list item
     */
    protected abstract ViewsHolder onViewHolderCreate(ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(ViewsHolder views, int position) {
        if (views.getItemViewType() == HEADER) {
            setHeaderData(views);
            setHeaderActions(views);
        } else if (views.getItemViewType() == FOOTER) {
            setFooterData(views);
            setFooterActions(views);
        } else {
            if (getHeaderLayoutId() != 0)
                onViewHolderBind(views, position - 1);
            else
                onViewHolderBind(views, position);
        }
    }

    /**
     * The method where you can set the data and the functionalities for each item in the list
     *
     * @param views    The {@link ViewsHolder ViewsHolder} object for the current item which contains all of the views from the item layout
     * @param position The position of the current item in the list
     */
    protected abstract void onViewHolderBind(ViewsHolder views, int position);

    @Override
    public int getItemViewType(int position) {
        if (getHeaderLayoutId() != 0 && position == 0) {
            return HEADER;
        } else if (getFooterLayoutId() != 0 && position == getItemCount() - 1) {
            return FOOTER;
        } else {
            return getHeaderLayoutId() != 0 ? getViewType(position - 1) : getViewType(position);
        }
    }

    /**
     * The method that returns the view type of the current item
     *
     * @param position The position of the current item in the list
     * @return The view type of the current item
     */
    protected int getViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        int count = getTotalItemCount();

        if (getHeaderLayoutId() != 0)
            count++;

        if (getFooterLayoutId() != 0)
            count++;

        return count;
    }

    /**
     * The method that returns the total number of items in the list
     *
     * @return Total number of items in the list
     */
    protected abstract int getTotalItemCount();

    /**
     * The method that return the layout id of the header
     *
     * @return The layout id of the header
     */
    protected int getHeaderLayoutId() {
        return 0;
    }

    /**
     * The method that sets the data in the layout of the header
     *
     * @param views The {@link ViewsHolder ViewsHolder} object for the views in the header layout
     */
    protected void setHeaderData(ViewsHolder views) {

    }

    /**
     * The method that sets the functionalities of the views in the layout of the header
     *
     * @param views The {@link ViewsHolder ViewsHolder} object for the views in the header layout
     */
    protected void setHeaderActions(ViewsHolder views) {

    }

    /**
     * The method that return the layout id of the footer
     *
     * @return The layout id of the footer
     */
    protected int getFooterLayoutId() {
        return 0;
    }

    /**
     * The method that sets the data in the layout of the footer
     *
     * @param views The {@link ViewsHolder ViewsHolder} object for the views in the footer layout
     */
    protected void setFooterData(ViewsHolder views) {

    }

    /**
     * The method that sets the functionalities of the views in the layout of the footer
     *
     * @param views The {@link ViewsHolder ViewsHolder} object for the views in the footer layout
     */
    protected void setFooterActions(ViewsHolder views) {

    }

    /**
     * The method that returns the adapter position of the {@link ViewsHolder ViewsHolder} object passed as a parameter like the list had no header attached
     *
     * @param views The {@link ViewsHolder ViewsHolder} object from which you want to find its adapter position
     * @return The position in the list adapter like the list does not have any header
     */
    protected int getAdapterPosition(ViewsHolder views) {
        return getHeaderLayoutId() != 0 ? views.getAdapterPosition() - 1 : views.getAdapterPosition();
    }


    /**
     * <b>!!!TO BE USED IF YOU HAVE HEADER OF FOOTER!!!</b>
     * <p>For documentation on what this method does see {@link #notifyItemRangeInserted(int, int) notifyItemRangeInserted}</p>
     */
    public void notifyItemRangeWasInserted(int positionStart, int itemCount) {
        if (getHeaderLayoutId() != 0)
            notifyItemRangeInserted(positionStart + 1, itemCount);
        else
            notifyItemRangeInserted(positionStart, itemCount);
    }

    /**
     * <b>!!!TO BE USED IF YOU HAVE HEADER OF FOOTER!!!</b>
     * <p>For documentation on what this method does see {@link #notifyItemRangeRemoved(int, int) notifyItemRangeRemoved}</p>
     */
    public void notifyItemRangeWasRemoved(int positionStart, int itemCount) {
        if (getHeaderLayoutId() != 0)
            notifyItemRangeRemoved(positionStart + 1, itemCount);
        else
            notifyItemRangeRemoved(positionStart, itemCount);
    }

    /**
     * <b>!!!TO BE USED IF YOU HAVE HEADER OF FOOTER!!!</b>
     * <p>For documentation on what this method does see {@link #notifyItemChanged(int) notifyItemChanged}</p>
     */
    public void notifyItemWasChanged(int position) {
        if (getHeaderLayoutId() != 0)
            notifyItemChanged(position + 1);
        else
            notifyItemChanged(position);
    }

    /**
     * <b>!!!TO BE USED IF YOU HAVE HEADER OF FOOTER!!!</b>
     * <p>For documentation on what this method does see {@link #notifyItemInserted(int) notifyItemInserted}</p>
     */
    public void notifyItemWasInserted(int position) {
        if (getHeaderLayoutId() != 0)
            notifyItemInserted(position + 1);
        else
            notifyItemInserted(position);
    }

    /**
     * <b>!!!TO BE USED IF YOU HAVE HEADER OF FOOTER!!!</b>
     * <p>For documentation on what this method does see {@link #notifyItemMoved(int, int) notifyItemMoved}</p>
     */
    public void notifyItemWasMoved(int fromPosition, int toPosition) {
        if (getHeaderLayoutId() != 0)
            notifyItemMoved(fromPosition + 1, toPosition + 1);
        else
            notifyItemMoved(fromPosition, toPosition);
    }

    /**
     * <b>!!!TO BE USED IF YOU HAVE HEADER OF FOOTER!!!</b>
     * <p>For documentation on what this method does see {@link #notifyItemRangeChanged(int, int) notifyItemRangeChanged}</p>
     */
    public void notifyItemRangeWasChanged(int positionStart, int itemCount) {
        if (getHeaderLayoutId() != 0)
            notifyItemRangeChanged(positionStart + 1, itemCount);
        else
            notifyItemRangeChanged(positionStart, itemCount);
    }

    /**
     * <b>!!!TO BE USED IF YOU HAVE HEADER OF FOOTER!!!</b>
     * <p>For documentation on what this method does see {@link #notifyItemRemoved(int) notifyItemRemoved}</p>
     */
    public void notifyItemWasRemoved(int position) {
        if (getHeaderLayoutId() != 0)
            notifyItemRemoved(position + 1);
        else
            notifyItemRemoved(position);
    }

    /**
     * Notifies the header that the his data changed
     */
    public void notifyHeaderChanged() {
        if (getHeaderLayoutId() != 0)
            notifyItemChanged(0);
    }

    /**
     * Notifies the footer that his data changed
     */
    public void notifyFooterChanged() {
        if (getFooterLayoutId() != 0)
            notifyItemChanged(getItemCount() - 1);
    }
}
