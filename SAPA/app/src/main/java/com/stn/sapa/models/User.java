package com.stn.sapa.models;

import java.io.Serializable;

/**
 * Created by Sebastian on 1/31/2017.
 */

public class User extends UserProfile implements Serializable {

    private String id;
    private String email;
    private boolean registered;
    private UserExtra extra;


    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public boolean isRegistered() {
        return registered;
    }

    public UserExtra getExtra() {
        return extra;
    }
}
