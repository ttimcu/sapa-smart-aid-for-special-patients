package com.stn.sapa.retrofit.requestbodies;

/**
 * Created by Sebastian on 1/29/2017.
 */

public class UserRegistrationVerifyCodeBody {

    private String code;

    public UserRegistrationVerifyCodeBody(String code) {
        this.code = code;
    }
}
