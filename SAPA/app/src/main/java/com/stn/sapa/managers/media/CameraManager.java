package com.stn.sapa.managers.media;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.stn.sapa.constants.ActivityResultContants;
import com.stn.sapa.utils.CompressUtils;
import com.stn.sapa.utils.MediaFileUtils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Sebastian on 7/8/2016.
 */
class CameraManager {

    private Activity activity;
    private MediaManager.OnMediaItemReceivedCallback<File> callback;

    private Uri currentMediaFileUri;

    private File mediaFile;

    CameraManager(Activity activity) {
        this.activity = activity;
    }

    void capturePhoto(MediaManager.OnMediaItemReceivedCallback<File> callback) {
        this.callback = callback;

        if (this.callback != null) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                // Create the File where the photo should go
                try {
                    mediaFile = MediaFileUtils.createMediaPhotoTempFile(activity);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    ex.printStackTrace();
                }

                // Continue only if the File was successfully created
                if (mediaFile != null) {
                    currentMediaFileUri = FileProvider.getUriForFile(activity, "com.stn.sapa.fileprovider", mediaFile);

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentMediaFileUri);

                    activity.startActivityForResult(takePictureIntent, ActivityResultContants.CAPTURE_IMAGE_REQUEST_CODE);
                }
            }
        }
    }

    void onActivityResult(int requestCode, int resultCode) {
        if (callback != null && resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ActivityResultContants.CAPTURE_IMAGE_REQUEST_CODE:
                    CompressUtils.compressPhoto(activity, currentMediaFileUri, new CompressUtils.OnCompressionFinishedCallback<File>() {
                        @Override
                        public void onCompressionFinished(File compressedObject) {
                            FileUtils.deleteQuietly(mediaFile);

                            callback.onMediaItemReceived(compressedObject);
                        }
                    });
                    break;
            }
        }
    }
}
