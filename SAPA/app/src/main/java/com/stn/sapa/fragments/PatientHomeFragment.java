package com.stn.sapa.fragments;

import com.stn.sapa.z_base.BaseFragment;

/**
 * Created by Sebastian on 2/1/2017.
 */

public class PatientHomeFragment extends BaseFragment {
    @Override
    protected int getFragmentLayoutId() {
        return 0;
    }

    @Override
    protected void linkUI() {

    }

    @Override
    protected void init() {

    }

    @Override
    protected void setData() {

    }

    @Override
    protected void setActions() {

    }
}
