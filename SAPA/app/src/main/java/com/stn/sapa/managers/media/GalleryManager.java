package com.stn.sapa.managers.media;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.stn.sapa.constants.ActivityResultContants;
import com.stn.sapa.utils.CompressUtils;

import java.io.File;


/**
 * Created by Sebastian on 7/9/2016.
 */
class GalleryManager {

    private Activity activity;
    private MediaManager.OnMediaItemReceivedCallback<File> callback;

    GalleryManager(Activity activity) {
        this.activity = activity;
    }

    void openPhotoGallery(MediaManager.OnMediaItemReceivedCallback<File> callback) {
        this.callback = callback;

        Intent intent = new Intent();

        intent.setType("image/*");

        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        activity.startActivityForResult(intent, ActivityResultContants.GALLERY_PHOTO_REQUEST_CODE);
    }

    void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callback != null && resultCode == Activity.RESULT_OK && data != null) {
            final Uri selectedFileUri = data.getData();

            if (selectedFileUri != null)
                switch (requestCode) {
                    case ActivityResultContants.GALLERY_PHOTO_REQUEST_CODE:
                        CompressUtils.compressPhoto(activity, selectedFileUri, new CompressUtils.OnCompressionFinishedCallback<File>() {
                            @Override
                            public void onCompressionFinished(File compressedObject) {
                                callback.onMediaItemReceived(compressedObject);
                            }
                        });
                        break;
                }
        }
    }
}
