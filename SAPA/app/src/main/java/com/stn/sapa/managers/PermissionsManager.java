package com.stn.sapa.managers;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.stn.sapa.constants.PermissionRequestsContants;


/**
 * Created by Sebastian on 9/28/2016.
 */

public class PermissionsManager {

    private static PermissionsManager instance;
    private static Activity currentActivity;

    private OnPermissionsCallback writeExternalStoragePermissionCallback;
    private OnPermissionsCallback recordAudioPermissionCallback;

    public static PermissionsManager getInstance(Activity activity) {
        if (instance == null || !currentActivity.getLocalClassName().contentEquals(activity.getLocalClassName()) || currentActivity.isFinishing()) {
            currentActivity = activity;
            instance = new PermissionsManager();
        }

        return instance;
    }

    private PermissionsManager() {

    }

    public void checkRecordAudioPermission(OnPermissionsCallback callback) {
        this.recordAudioPermissionCallback = callback;

        if (recordAudioPermissionCallback != null) {
            if (ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.RECORD_AUDIO}, PermissionRequestsContants.PERMISSION_RECORD_AUDIO_REQUEST_CODE);
            } else {
                recordAudioPermissionCallback.onPermissionGranted();
                recordAudioPermissionCallback = null;
            }
        }
    }

    public void checkWriteExternalStoragePermission(OnPermissionsCallback callback) {
        this.writeExternalStoragePermissionCallback = callback;

        if (writeExternalStoragePermissionCallback != null) {
            if (ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PermissionRequestsContants.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
            } else {
                writeExternalStoragePermissionCallback.onPermissionGranted();
                writeExternalStoragePermissionCallback = null;
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionRequestsContants.PERMISSION_RECORD_AUDIO_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkRecordAudioPermission(recordAudioPermissionCallback);
                } else {
                    recordAudioPermissionCallback.onPermissionRejected();
                    recordAudioPermissionCallback = null;
                }
                break;

            case PermissionRequestsContants.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkWriteExternalStoragePermission(writeExternalStoragePermissionCallback);
                } else {
                    writeExternalStoragePermissionCallback.onPermissionRejected();
                    writeExternalStoragePermissionCallback = null;
                }
                break;
        }
    }

    public interface OnPermissionsCallback {
        void onPermissionGranted();

        void onPermissionRejected();
    }
}
