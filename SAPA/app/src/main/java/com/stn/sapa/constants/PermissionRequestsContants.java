package com.stn.sapa.constants;

/**
 * Created by Sebastian on 9/28/2016.
 */

public class PermissionRequestsContants {
    public static final int PERMISSION_RECORD_AUDIO_REQUEST_CODE = 100;
    public static final int PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 101;
    public static final int PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE = 102;
}
