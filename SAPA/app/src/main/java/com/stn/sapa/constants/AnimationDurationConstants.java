package com.stn.sapa.constants;

/**
 * Created by Sebastian on 9/21/2016.
 */
public class AnimationDurationConstants {

    public static final int DURATION_REVEAL_ACTIVITY = 300;
    public static final int ANIMATION_BUTTON_ALPHA = 300;
}
