package com.stn.sapa.z_base;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebi Ursulescu on 7/5/2015.
 */
public abstract class BaseFragmentsActivity extends BaseActivity {

    protected FragmentManager fragmentManager;
    protected ArrayList<WeakReference<Fragment>> fragments = new ArrayList<>();

    protected abstract int getFragmentContainerId();

    @Override
    protected void doStuffBeforeLinkUI() {
        fragmentManager = getFragmentManager();

        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });

    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        fragments.add(new WeakReference<>(fragment));
    }

    public List<Fragment> getFragments() {
        ArrayList<Fragment> ret = new ArrayList<>();
        for (WeakReference<Fragment> ref : fragments) {
            Fragment f = ref.get();
            if (f != null) {
                if (f.isVisible()) {
                    ret.add(f);
                }
            }
        }

        return ret;
    }

    public void setFragment(Fragment fragment) {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .replace(getFragmentContainerId(), fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();
        fragmentManager.executePendingTransactions();
    }

    public void addFragment(Fragment fragment) {
        fragmentManager.beginTransaction().add(getFragmentContainerId(), fragment, "" + System.currentTimeMillis()).addToBackStack(fragment.getTag()).commit();
        fragmentManager.executePendingTransactions();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 1)
            fragmentManager.popBackStack();
        else
            finish();
    }
}
